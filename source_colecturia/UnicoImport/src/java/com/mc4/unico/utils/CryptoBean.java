package com.mc4.unico.utils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/*******************************************************************************
 * Clase para encriptar cadenas con MD5
 * @author fernando flores
 * @version 1.0.0.090714
 ******************************************************************************/

public class CryptoBean  {

    /**
     * Encriptacion de cadenas.
     * @param strValue es la cadena a encriptar.
     * @param strKey es la clave de encriptacion.
     * @return UnicoResponse con la cadena encriptada o la descripcion ocurrido.
     */
    public String encrypt(String strValue, String strKey) {        
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digestOfPassword = md.digest(strKey.getBytes("utf-8"));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);

            SecretKey key = new SecretKeySpec(keyBytes, "DESede");
            Cipher cipher = Cipher.getInstance("DESede");
            cipher.init(Cipher.ENCRYPT_MODE, key);

            byte[] plainTextBytes = strValue.getBytes("utf-8");
            byte[] buf = cipher.doFinal(plainTextBytes);
            return Base64.encodeBase64String(buf);
            
        } catch (NoSuchAlgorithmException ex) {
            System.out.println(ex.getMessage());
            return "";
        } catch (UnsupportedEncodingException ex) {
            System.out.println(ex.getMessage());
            return "";
        } catch (NoSuchPaddingException ex) {
            System.out.println(ex.getMessage());
            return "";
        } catch (InvalidKeyException ex) {
            System.out.println(ex.getMessage());
            return "";
        } catch (IllegalBlockSizeException ex) {
            System.out.println(ex.getMessage());
            return "";
        } catch (BadPaddingException ex) {
            System.out.println(ex.getMessage());
            return "";
        }       
    }

    /**
     * Desencripta cadenas de texto
     * @param strValue es la cadena a desencriptar.
     * @param strKey es la clave de encriptacion.
     * @return UnicoResponse conteniendo la cadena desencriptada o la descripcion del error
     */
    public String decrypt(String strValue, String strKey) {
        try {
            
            byte[] message = Base64.decodeBase64(strValue.getBytes("utf-8")); 
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digestOfPassword = md.digest(strKey.getBytes("utf-8"));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
            SecretKey key = new SecretKeySpec(keyBytes, "DESede");

            Cipher decipher = Cipher.getInstance("DESede");
            decipher.init(Cipher.DECRYPT_MODE, key);

            byte[] plainText = decipher.doFinal(message);
            return new String(plainText, "UTF-8");
            
        } catch (UnsupportedEncodingException ex) {
            System.out.println(ex.getMessage());
            return "";
        } catch (NoSuchAlgorithmException ex) {
            System.out.println(ex.getMessage());
            return "";
        } catch (NoSuchPaddingException ex) {
            System.out.println(ex.getMessage());
            return "";
        } catch (InvalidKeyException ex) {
            System.out.println(ex.getMessage());
            return "";
        } catch (IllegalBlockSizeException ex) {
            System.out.println(ex.getMessage());
            return "";
        } catch (BadPaddingException ex) {
            System.out.println(ex.getMessage());
            return "";
        }
    }

}
