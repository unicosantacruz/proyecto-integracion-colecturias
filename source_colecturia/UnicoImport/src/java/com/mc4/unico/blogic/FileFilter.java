
package com.mc4.unico.blogic;

import java.io.File;
import java.io.FilenameFilter;

/*******************************************************************************
 * Clase para filtrar los nombres de archivo
 * @author fernando flores
 * @version 02082014
 ******************************************************************************/
public class FileFilter implements FilenameFilter {

    private String sufijo;
    private String prefijo;

    FileFilter(String prefijo, String sufijo) {
        this.prefijo = prefijo;
        this.sufijo = sufijo;
    }

    @Override
    public boolean accept(File dir, String name) {
        return name.endsWith(sufijo) && name.startsWith(prefijo);
    }
}
