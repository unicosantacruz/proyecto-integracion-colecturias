package com.mc4.unico.blogic;

import com.mc4.unico.proxy.ProxyFTPBean;
import com.mc4.unico.proxy.ProxySFTPBean;
import com.mc4.unico.proxy.ProxySMTPBean;
import com.mc4.unico.utils.CryptoBean;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import java.util.logging.Logger;

/**
 * *****************************************************************************
 * Clase que contiene la logica de importacion de archivo de datos
 *
 * @author fernando flores
 * @version 1.0.0.16072014
 * *****************************************************************************
 */
@Stateless
//@EJB(name = "AltServer", beanInterface = ImportBeanRemote.class)
public class ImportBean implements ImportBeanRemote {

    private final String CTE_PROTOCOL_SFTP = "SFTP";
    private final String CTE_PROTOCOL_FTP = "FTP";
    private final Integer CTE_FIELD_SIZE = 5;
    private final String CTE_COMPONENT_KEY = "UN1C0KEY";
    private final String CTE_ERROR_LABEL1 = "ERROR: ";
    private final String CTE_ERROR_LABEL2 = "ERROR: Ocurrió un error interno, revise los registros bitacora para mas detalle.";
    private static final Logger logger = Logger.getLogger(ImportBean.class.getName());

    /**
     * *************************************************************************
     * Contruye el nombre de archivos exportados en funcion de fecha y hora.
     *
     * @param strEmpresa Sufijo del nombre de archivo.
     * @return Retorna el nombre de archivo estandarizada.
     * ************************************************************************
     */
    private String buildFilename(String strEmpresa) {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Calendar calendar = Calendar.getInstance();
        return strEmpresa.toUpperCase() + "-" + dateFormat.format(calendar.getTime());
    }

    /**
     * *************************************************************************
     * Borra un archivo local de forma segura.
     *
     * @param strFullFilename Ruta completa del archivo a borrar.
     * ************************************************************************
     */
    private void removeFile(String strFullFilename) {
        try {
            File file = new File(strFullFilename);
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ImportBean.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    /**
     * *************************************************************************
     * Metodo mascara para enviar emails de alerta
     *
     * @param strSubject Asunto
     * @param strMessage Cuerpo del mensaje
     * @param lstRecipient Lista de destinatarios
     * ************************************************************************
     */
    private void sendMail(String strSubject, String strMessage, ArrayList<String> lstRecipient) {
        if (lstRecipient.size() > 0) {
            ProxySMTPBean email = new ProxySMTPBean();
            String[] vecRecipient = lstRecipient.toArray(new String[lstRecipient.size()]);
            String strResult = email.sendMail(strSubject, strMessage, vecRecipient);
            if (!strResult.equals("OK")) {
                logger.warning(strResult);
            }
        }
    }

    /**
     * *************************************************************************
     * Ejecuta la exportacion de archivo de pagos registrados.
     *
     * @param strEmpresa Nombre de la empresa registrado en la tabla BC_EMPRESA
     * @param strServicio Nombre del servicio registrado en la tabla BC_SERVICIO
     * @return "OK" mas un resumen del resultado
     * ************************************************************************
     */
    @Override
    public String export(String strEmpresa, String strServicio) {
        String result, query;
        Connection cnn = null;
        Statement stm = null, stmex = null;
        ResultSet rs = null, rsext = null;
        PreparedStatement prepared = null;
        Integer intSecuencia = 0, intIdPagoConfig = 0;
        String strLocalFilename = "", strLocalFilepath = "";
        FileWriter writer = null;
        ProxySFTPBean proxyssh = null;
        ProxyFTPBean proxyftp = null;
        boolean blnTransactionStarted = false;
        Integer intTotalRegistros = 0;
        ArrayList<String> lstEmailErr = new ArrayList<String>();
        ArrayList<String> lstEmailNot = new ArrayList<String>();
        StringBuilder bufEmailBody = new StringBuilder();

        try {
            //------------------------------------------------------------------
            // Cargo la configuracion del logger
            //------------------------------------------------------------------
            logger.log(Level.INFO, "INICIA EXPORTACION {0}", strEmpresa);
            //------------------------------------------------------------------
            // Conecto a la base de datos
            //------------------------------------------------------------------
            cnn = getConnection();
            stm = cnn.createStatement();
            stmex = cnn.createStatement();
            //------------------------------------------------------------------
            // Obtengo el codigo de servicio que relaciona servicio con la tabla de exportacion
            //------------------------------------------------------------------
            String strout = subServiceCode(stm, rs, strEmpresa, strServicio);
            //--------------------------------------------------------------
            // recupero los emails de envio
            //--------------------------------------------------------------
            query = "select e.direccion,a.alerta_errores,a.alerta_eventos from di_email_servicio a, di_email e where a.id_email=e.id_email and a.estado='ON' and a.cod_servicio='" + strout + "'";
            rsext = stmex.executeQuery(query);
            while (rsext.next()) {
                if (rsext.getInt("alerta_errores") > 0) {
                    lstEmailErr.add(rsext.getString("direccion"));
                }
                if (rsext.getInt("alerta_eventos") > 0) {
                    lstEmailNot.add(rsext.getString("direccion"));
                }
            }
            rsext.close();
            //------------------------------------------------------------------
            // Obtengo los datos de la tabla de configuracion de la exportacion
            //------------------------------------------------------------------
            query = "select c.protocolo,a.id_exportacion,a.id_conexion,a.extension_salida,a.directorio_salida,a.separador,a.directorio_temporal,a.tipo_salida,a.query_datos,a.query_nombre,a.filtro_cuenta,a.extension_proceso,a.descripcion from di_exportacion a ,di_conexion c where a.id_conexion=c.id_conexion and a.estado='ON' and a.cod_servicio='" + strout + "'";
            rsext = stmex.executeQuery(query);
            while (rsext.next()) {
                String strDescripcion = rsext.getString("descripcion");
                Integer intIdConexion = rsext.getInt("id_conexion");
                Integer intIdExportacion = rsext.getInt("id_exportacion");
                String strProtocol = rsext.getString("protocolo");
                String strSeparator = rsext.getString("separador").replace("\\t", "\t");
                String strTmpPath = rsext.getString("directorio_temporal");
                String strOutSufijo = rsext.getString("extension_salida");
                String strOutPath = rsext.getString("directorio_salida");
                String strTipoSalida = rsext.getString("tipo_salida");
                String strQueryDatos = rsext.getString("query_datos");
                String strQueryNombre = rsext.getString("query_nombre");
                String strFiltroCuenta = rsext.getString("filtro_cuenta");
                String strTmpSufijo = rsext.getString("extension_proceso");
                //------------------------------------------------------------------
                // valido campos nulos
                //------------------------------------------------------------------
                strTmpSufijo = (strTmpSufijo == null ? "" : strTmpSufijo);
                strOutSufijo = (strOutSufijo == null ? "" : strOutSufijo);
                strTmpPath = (strTmpPath == null ? "" : strTmpPath);
                //------------------------------------------------------------------
                // cargando el contenido del email de notificacion
                //------------------------------------------------------------------
                bufEmailBody.append(strDescripcion);
                //------------------------------------------------------------------
                // Obtengo los parametros de conexion al repositorio de archivos
                //------------------------------------------------------------------
                Map<String, String> vecConnData = subConnectionData(stm, rs, intIdConexion);
                //------------------------------------------------------------------
                // Cuento cuantos pagos por exportar existen
                //------------------------------------------------------------------
                query = "select count(p.id_pago) from di_pagos p, tx_pagos x where x.id_pago=p.id_pago and x.sq_exportacion=0 and p.cod_servicio='" + strout + "'";
                if (strFiltroCuenta != null) {
                    query += " and x.cuenta='" + strFiltroCuenta + "'";
                }
                rs = stm.executeQuery(query);
                if (!rs.next()) {
                    throw new Exception("No se encontró la transaccion para el servicio " + strout);
                }
                intSecuencia = rs.getInt(1);
                rs.close();
                //------------------------------------------------------------------
                // Si hay pagos que exportar
                //------------------------------------------------------------------
                if (intSecuencia > 0) {
                    //------------------------------------------------------------------
                    // Guardo la hora de inicio del proceso
                    //------------------------------------------------------------------
                    java.sql.Timestamp sql1Timestamp = null, sql2Timestamp = null;
                    sql1Timestamp = getCurrentTimeStamp();
                    //------------------------------------------------------------------
                    // Obtengo el ID de la configuracion de pagos
                    //------------------------------------------------------------------
                    query = "select p.id_pago from di_pagos p where p.cod_servicio='" + strout + "'";
                    rs = stm.executeQuery(query);
                    if (!rs.next()) {
                        throw new Exception("No se encontró la configuracion de pago para el servicio " + strout);
                    }
                    intIdPagoConfig = rs.getInt(1);
                    rs.close();
                    //------------------------------------------------------------------
                    // Obtengo el numero de secuencia para la exportacion
                    //------------------------------------------------------------------
                    query = "select sec_exportacion.nextval from dual";
                    rs = stm.executeQuery(query);
                    if (!rs.next()) {
                        throw new Exception("Error al obtener el número de secuencia.");
                    }
                    intSecuencia = rs.getInt(1);
                    rs.close();
                    //------------------------------------------------------------------
                    // Marco los registros a exportar con el numero de secuancia como agrupador
                    //------------------------------------------------------------------
                    blnTransactionStarted = true;
                    query = "update tx_pagos set sq_exportacion=" + intSecuencia.toString() + " where sq_exportacion=0 and id_pago=" + intIdPagoConfig.toString();
                    if (strFiltroCuenta != null) {
                        query += " and cuenta='" + strFiltroCuenta + "'";
                    }
                    stm.executeUpdate(query);
                    cnn.commit();
                    blnTransactionStarted = false;
                    //------------------------------------------------------------------
                    // Defino los formatos de horas y dinero
                    //------------------------------------------------------------------
                    DateFormat dateFormat = new SimpleDateFormat("HH:mm dd-MM-yyyy");
                    DecimalFormat decimalFormat = new DecimalFormat("#########0.00");
                    //------------------------------------------------------------------
                    // Defino los nombres de archivo local temporal y el remoto
                    //------------------------------------------------------------------
                    if (strQueryNombre == null) {
                        strLocalFilename = buildFilename(strEmpresa);
                    } else {
                        rs = stm.executeQuery(strQueryNombre);
                        if (!rs.next()) {
                            throw new Exception("Error al obtener el nombre del archivo de salida.");
                        }
                        strLocalFilename = rs.getString(1);
                        rs.close();
                    }
                    String strFilenameA = strLocalFilename + strTmpSufijo;
                    String strFilenameB = strLocalFilename + strOutSufijo;
                    if (strTmpPath.length() > 0) {
                        strLocalFilepath = strTmpPath + "/" + strFilenameA;
                    } else {
                        strLocalFilepath = strFilenameA;
                    }
                    //------------------------------------------------------------------
                    // Creo el archivo temporal local
                    //------------------------------------------------------------------
                    writer = new FileWriter(strLocalFilepath, false);
                    if (writer == null) {
                        throw new Exception("No pudo crearse el archivo temporal: " + strLocalFilepath);
                    }
                    //------------------------------------------------------------------
                    // Recorrer todas los pagos e ir escribiendo en el archivo
                    //------------------------------------------------------------------
                    if (strQueryDatos == null) {
                        query = "select clave1,monto,fecha_pago,nro_cliente,nro_ptm from tx_pagos where sq_exportacion=" + intSecuencia.toString() + " order by fecha_pago asc";
                        rs = stm.executeQuery(query);
                        while (rs.next()) {
                            writer.write(rs.getString("clave1"));
                            writer.write(strSeparator);
                            writer.write(decimalFormat.format(rs.getDouble("monto")));
                            writer.write(strSeparator);
                            writer.write(dateFormat.format(rs.getDate("fecha_pago")));
                            writer.write(strSeparator);
                            if (rs.getString("nro_cliente") != null) {
                                writer.write(rs.getString("nro_cliente"));
                            }
                            writer.write(strSeparator);
                            if (rs.getString("nro_ptm") != null) {
                                writer.write(rs.getString("nro_ptm"));
                            }
                            writer.write("\r\n");
                            writer.flush();
                            intTotalRegistros++;
                        }
                        rs.close();
                    } else {
                        query = strQueryDatos.toLowerCase();
                        if (query.contains(" where ")) {
                            query = strQueryDatos + " and sq_exportacion=" + intSecuencia.toString();
                        } else {
                            query = strQueryDatos + " where sq_exportacion=" + intSecuencia.toString();
                        }
                        rs = stm.executeQuery(query);
                        while (rs.next()) {
                            writer.write(rs.getString(1));
                            writer.write("\r\n");
                            writer.flush();
                            intTotalRegistros++;
                        }
                        rs.close();
                    }
                    //------------------------------------------------------------------
                    // Cierro el archivo
                    //------------------------------------------------------------------
                    writer.close();
                    //------------------------------------------------------------------
                    // Procesar segun el protocolo de comunicacion configurado
                    //------------------------------------------------------------------
                    if (!strProtocol.equals(CTE_PROTOCOL_SFTP) && !strProtocol.equals(CTE_PROTOCOL_FTP)) {
                        throw new Exception("No se reconoce el protocolo: " + strProtocol);
                    }
                    //------------------------------------------------------------------
                    // crear la conexion SFTP 
                    //------------------------------------------------------------------
                    try {
                        if (strProtocol.equals(CTE_PROTOCOL_FTP)) {
                            proxyftp = new ProxyFTPBean();
                            if (!proxyftp.connect(vecConnData.get("host"), vecConnData.get("username"), vecConnData.get("password"), Integer.parseInt(vecConnData.get("port")), strOutPath)) {
                                throw new Exception("Error en la conexion al repositorio de archivos : " + proxyftp.getLastError());
                            }
                        } else {
                            proxyssh = new ProxySFTPBean();
                            if (!proxyssh.connect(vecConnData.get("host"), vecConnData.get("username"), vecConnData.get("password"), Integer.parseInt(vecConnData.get("port")), strOutPath)) {
                                throw new Exception("Error en la conexion al repositorio de archivos : " + proxyssh.getLastError());
                            }
                        }
                    } catch (Exception loc) {
                        throw new Exception(loc.getMessage());
                    }
                    //------------------------------------------------------------------
                    // Mandar el archivo al repositorio remoto
                    //------------------------------------------------------------------
                    if (strOutPath != null && strOutPath.length() > 0) {
                        if (proxyftp != null) {
                            proxyftp.changeDir(strOutPath);
                        } else if (proxyssh != null) {
                            proxyssh.changeDir(strOutPath);
                        }
                    }
                    if (strTipoSalida.equals("TXT")) {
                        if (proxyftp != null) {
                            if (!proxyftp.putFile(strFilenameA, strTmpPath)) {
                                throw new Exception("Error al exportar el archivo '" + strLocalFilename + "': " + proxyftp.getLastError());
                            }
                            proxyftp.remFile(strFilenameB);
                            proxyftp.renFile(strFilenameA, strFilenameB);
                        } else if (proxyssh != null) {
                            if (!proxyssh.putFile(strFilenameA, strTmpPath)) {
                                throw new Exception("Error al exportar el archivo '" + strLocalFilename + "': " + proxyssh.getLastError());
                            }
                            proxyssh.remFile(strFilenameB);
                            proxyssh.renFile(strFilenameA, strFilenameB);
                        }
                        removeFile(strLocalFilepath);
                    } else if (strTipoSalida.equals("ZIP")) {
                        String strZipFilename = changeFileExtension(strFilenameB, strOutSufijo, ".zip");
                        String strFullBFile = strTmpPath + "/" + strFilenameB;
                        String strFullZFile = strTmpPath + "/" + strZipFilename;
                        File fileA = new File(strLocalFilepath);
                        File fileB = new File(strFullBFile);
                        fileA.renameTo(fileB);
                        ZipFile zipFile2 = new ZipFile(strFullZFile);
                        ArrayList filesToAdd = new ArrayList();
                        filesToAdd.add(fileB);
                        //filesToAdd.add(new File("C:\\Users\\fer\\Documents\\SFTP-Location\\zip\\salida\\6.bmp"));
                        ZipParameters parameters = new ZipParameters();
                        parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
                        parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
                        zipFile2.addFiles(filesToAdd, parameters);
                        if (proxyftp != null) {
                            proxyftp.remFile(strZipFilename);
                            if (!proxyftp.putFile(strZipFilename, strTmpPath)) {
                                throw new Exception("Error al exportar el archivo '" + strLocalFilename + "': " + proxyftp.getLastError());
                            }
                        } else if (proxyssh != null) {
                            proxyssh.remFile(strZipFilename);
                            if (!proxyssh.putFile(strZipFilename, strTmpPath)) {
                                throw new Exception("Error al exportar el archivo '" + strLocalFilename + "': " + proxyssh.getLastError());
                            }
                        }
                        removeFile(strFullZFile);
                    }
                    //------------------------------------------------------------------
                    // Detengo cronometro del proceso
                    //------------------------------------------------------------------
                    sql2Timestamp = getCurrentTimeStamp();
                    //------------------------------------------------------------------
                    // registro la exportacion del archivo
                    //------------------------------------------------------------------
                    blnTransactionStarted = true;
                    query = "insert into tx_exportacion (id_exportacion,nombre_archivo,registros,fecha_inicio,fecha_final,sq_exportacion) values (?,?,?,?,?,?)";
                    prepared = cnn.prepareStatement(query);
                    prepared.setInt(1, intIdExportacion);
                    prepared.setString(2, strFilenameB);
                    prepared.setInt(3, intTotalRegistros);
                    prepared.setTimestamp(4, sql1Timestamp);
                    prepared.setTimestamp(5, sql2Timestamp);
                    prepared.setInt(6, intSecuencia);
                    prepared.execute();
                    cnn.commit();
                    //------------------------------------------------------------------
                    // Actualizo el estado de los pagos de NEW -> EXP
                    //------------------------------------------------------------------
                    query = "update tx_pagos set estado='EXP' where sq_exportacion=" + intSecuencia.toString();
                    stm.executeUpdate(query);
                    cnn.commit();
                    blnTransactionStarted = false;
                    //------------------------------------------------------------------
                    // Envio de notificaciones por email
                    //------------------------------------------------------------------
                    bufEmailBody.append(" : ").append(strFilenameB).append(" OK.\r\n");
                }
            } // loop de configuracion de exportacion
            rsext.close();
            //------------------------------------------------------------------
            // Retorno OK si el proceso fue exitoso
            //------------------------------------------------------------------
            if (intTotalRegistros > 0) {
                result = "OK " + intTotalRegistros.toString() + " registros cargados.";
                sendMail("Exportacion " + strEmpresa + " concluida satisfactoriamente.", bufEmailBody.toString(), lstEmailNot);
            } else {
                result = "OK";
            }
            //------------------------------------------------------------------
            // cierro db
            //------------------------------------------------------------------
            cnn.close();
        } catch (SQLException sqlex) {
            try {
                if (cnn != null) {
                    cnn.rollback();
                }
            } catch (SQLException loc) {
                Logger.getLogger(ImportBean.class.getName()).log(Level.SEVERE, null, loc);
            }
            result = CTE_ERROR_LABEL1 + sqlex.getMessage();
            logger.severe(sqlex.getMessage());
            sendMail("Exportacion " + strEmpresa + " abortada por errores en base de datos", bufEmailBody.append("\r\n").toString() + sqlex.getMessage(), lstEmailErr);
            System.out.println(sqlex.getMessage());
        } catch (Exception ex) {
            try {
                if (cnn != null) {
                    cnn.rollback();
                }
            } catch (SQLException loc) {
                Logger.getLogger(ImportBean.class.getName()).log(Level.SEVERE, null, loc);
            }
            result = CTE_ERROR_LABEL1 + ex.getMessage();
            logger.severe(ex.getMessage());
            sendMail("Exportacion " + strEmpresa + " abortada por errores en el proceso", bufEmailBody.append("\r\n").toString() + ex.getMessage(), lstEmailErr);
            System.out.println(ex.getMessage());
        } finally {
            //------------------------------------------------------------------
            // Desconexion SFTP
            //------------------------------------------------------------------
            if (proxyftp != null) {
                proxyftp.disconnect();
            }
            if (proxyssh != null) {
                proxyssh.disconnect();
            }
            //------------------------------------------------------------------
            // Desconexion db
            //------------------------------------------------------------------
            try {
                if (cnn != null) {
                    if (blnTransactionStarted && !cnn.isClosed()) {
                        cnn.rollback();
                    }
                    if (prepared != null) {
                        if (!prepared.isClosed()) {
                            prepared.close();
                        }
                    }
                    if (stmex != null) {
                        if (rsext != null) {
                            if (!rsext.isClosed()) {
                                rsext.close();
                            }
                        }
                        if (!stmex.isClosed()) {
                            stmex.close();
                        }
                    }
                    if (stm != null) {
                        if (rs != null) {
                            if (!rs.isClosed()) {
                                rs.close();
                            }
                        }
                        if (!stm.isClosed()) {
                            stm.close();
                        }
                    }
                    if (!cnn.isClosed()) {
                        cnn.close();
                    }
                }
            } catch (SQLException sqlex) {
                java.util.logging.Logger.getLogger(ImportBean.class.getName()).log(java.util.logging.Level.SEVERE, null, sqlex);
            }
            logger.log(Level.INFO, "TERMINA EXPORTACION {0}", strEmpresa);
        }
        return result;
    }

    /**
     * *************************************************************************
     * Procedimiento para obtener el codigo del servicio.
     *
     * @param stm Objeto statement
     * @param rs Objeto resultset
     * @param strEmpresa Nombre de la empresa configurada en la tabla BC_EMPRESA
     * @param strServicio Nombre del servicio configurada en la tabla
     * BC_SERVICIO
     * @return Retorna el codigo del servicio.
     * @throws SQLException
     * @throws Exception
     * ************************************************************************
     */
    private String subServiceCode(Statement stm, ResultSet rs, String strEmpresa, String strServicio) throws SQLException, Exception {
        String query = "select s.cod_servicio from bc_empresa e, bc_servicio s where e.cod_empresa=s.cod_empresa and s.descripcion='" + strServicio + "' and e.descripcion='" + strEmpresa + "'";
        rs = stm.executeQuery(query);
        if (!rs.next()) {
            throw new Exception("El servicio: '" + strServicio + "' o la empresa: '" + strEmpresa + "' no estan configurado.");
        }
        query = rs.getString("cod_servicio");
        rs.close();
        return query;
    }

    /**
     * *************************************************************************
     * Procedimiento para leer y devolver los parameros de conexion.
     *
     * @param stm Objeto statement
     * @param rs Objeto resultset
     * @param intIdConexion ID de la conexion configurada.
     * @return Retorna un HashMap con los parametros de conexion cargados.
     * @throws SQLException
     * @throws Exception
     * ************************************************************************
     */
    private Map<String, String> subConnectionData(Statement stm, ResultSet rs, Integer intIdConexion) throws SQLException, Exception {
        Map<String, String> result = new HashMap<String, String>();
        //----------------------------------------------------------------------
        // Consulto la base de datos
        //----------------------------------------------------------------------
        String query = "select nombre,valor,encriptado from di_atributo_conexion where id_conexion=" + intIdConexion.toString();
        rs = stm.executeQuery(query);
        while (rs.next()) {
            if (rs.getInt("encriptado") == 0) {
                //----------------------------------------------------------------------
                // Si el valor no esta encriptado, lo almaceno tal cual
                //----------------------------------------------------------------------
                result.put(rs.getString("nombre"), rs.getString("valor"));
            } else {
                //----------------------------------------------------------------------
                // Si el valor esta encriptado, lo desencripto y regitro
                //----------------------------------------------------------------------
                CryptoBean crypto = new CryptoBean();
                String response = crypto.decrypt(rs.getString("valor"), CTE_COMPONENT_KEY);
                result.put(rs.getString("nombre"), response);
            }
        }
        rs.close();
        //----------------------------------------------------------------------
        // Retorno el resultado
        //----------------------------------------------------------------------
        return result;
    }

    /**
     * *************************************************************************
     * Funcion para cambiar la extension del nombre de archivos
     *
     * @param strFilename nombre de archivo
     * @param strCurExtension extension actual
     * @param strNewExtension nueva extension
     * @return Nombre del archivo modificado
     * ************************************************************************
     */
    private String changeFileExtension(String strFilename, String strCurExtension, String strNewExtension) {
        String result = strFilename;
        if (strCurExtension != null) {
            if (result.length() > strCurExtension.length()) {
                result = result.substring(0, result.length() - strCurExtension.length());
            }
        }
        if (strNewExtension != null) {
            result += strNewExtension;
        }
        return result;
    }

    /**
     * *************************************************************************
     * Descomprime un paquete zip
     *
     * @param strSrcFilename nombre del archivo comprimido
     * @param strDestDirectory directorio donde se encuentra
     * @return true en caso de exito
     * ************************************************************************
     */
    private boolean unzip(String strSrcFilename, String strDestDirectory) {
        boolean result = false;
        try {
            ZipFile zipFile1 = new ZipFile(strSrcFilename);
            zipFile1.extractAll(strDestDirectory);
            result = true;
        } catch (ZipException ex) {
            java.util.logging.Logger.getLogger(ImportBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * *************************************************************************
     * Metodo para importar los archivos de datos
     *
     * @param strEmpresa Nombre de la empresa configurado en la tabla BC_EMPRESA
     * @param strServicio Nombre del servicio configurado en la tabla
     * BC_SERVICIO
     * @return Retorna el resultado de la operacion o la descripcion del error
     * ocurrido
     * ************************************************************************
     */
    @Override
    @SuppressWarnings("SleepWhileInLoop")
    public String collect(String strEmpresa, String strServicio) {
        String result = "OK", query = "", strout = "";
        Connection connection = null;
        Statement statement = null, stmex = null;
        ResultSet resultset = null;
        ResultSet rsnivel1 = null;
        ProxyFTPBean proxyftp = null;
        ProxySFTPBean proxyssh = null;
        PreparedStatement prepared = null;
        int intFilesToLoad = 0, intFilesLoaded = 0, intTotalRowsLoaded = 0;
        ArrayList<String> lstEmailErr = new ArrayList<String>();
        ArrayList<String> lstEmailNot = new ArrayList<String>();
        StringBuilder bufEmailBody = new StringBuilder();
        String strDMLError = null;
        boolean blnErrorOcurred = false;

        try {
            //------------------------------------------------------------------
            // Inicializo el logger
            //------------------------------------------------------------------
            logger.log(Level.INFO, "INICIA IMPORTACION {0}", strEmpresa);
            //------------------------------------------------------------------
            // crear la conexion a la base de datos
            //------------------------------------------------------------------
            connection = getConnection();
            statement = connection.createStatement();
            stmex = connection.createStatement();
            //------------------------------------------------------------------
            // obtengo el codigo del servicio para relacionar con la tabla de importacion
            // al mismo tiempo valido la configuracion de los argumentos de entrada
            //------------------------------------------------------------------
            strout = subServiceCode(statement, resultset, strEmpresa, strServicio);
            //------------------------------------------------------------------
            // bloqueo los pagos
            //------------------------------------------------------------------
            //statement.execute("update di_pagos set estado='OF' where cod_servicio='"+strout+"'");
            //connection.commit();
            //--------------------------------------------------------------
            // recupero los emails de envio
            //--------------------------------------------------------------
            query = "select e.direccion,a.alerta_errores,a.alerta_eventos from di_email_servicio a, di_email e where a.id_email=e.id_email and a.estado='ON' and a.cod_servicio='" + strout + "'";
            rsnivel1 = stmex.executeQuery(query);
            while (rsnivel1.next()) {
                if (rsnivel1.getInt("alerta_errores") > 0) {
                    lstEmailErr.add(rsnivel1.getString("direccion"));
                }
                if (rsnivel1.getInt("alerta_eventos") > 0) {
                    lstEmailNot.add(rsnivel1.getString("direccion"));
                }
            }
            rsnivel1.close();
            //------------------------------------------------------------------
            // obtengo todos los parametros de conexion y carga
            //------------------------------------------------------------------
            query = "select c.id_conexion,c.protocolo,a.id_importacion,a.separador,a.prefijo_entrada,a.extension_entrada,a.directorio_entrada,a.extension_salida,a.directorio_salida,a.linea_inicial,a.dmlupdate,a.dmldelete,a.dmlinsert,a.dmlinicial,a.extension_rechazo,a.directorio_rechazo,a.ordenacion,a.directorio_temporal,a.tipo_entrada,a.prefijo_unzip,a.extension_unzip,a.bloque_transaccion,a.limite_errores,a.extension_proceso,a.descripcion,a.dmlerror from di_importacion a, di_conexion c where a.id_conexion=c.id_conexion and a.estado='ON' and a.cod_servicio='" + strout + "'";
            rsnivel1 = stmex.executeQuery(query);
            while (rsnivel1.next()) {
                String strDescripcion = rsnivel1.getString("descripcion");
                Integer intIdConexion = rsnivel1.getInt("id_conexion");
                Integer intIdImportacion = rsnivel1.getInt("id_importacion");
                String strProtocol = rsnivel1.getString("protocolo");
                String strSeparator = rsnivel1.getString("separador").replace("\\t", "\t");
                String strInPrefijo = rsnivel1.getString("prefijo_entrada");
                String strInSufijo = rsnivel1.getString("extension_entrada");
                String strInPath = rsnivel1.getString("directorio_entrada");
                String strOutSufijo = rsnivel1.getString("extension_salida");
                String strOutPath = rsnivel1.getString("directorio_salida");
                String strRejSufijo = rsnivel1.getString("extension_rechazo");
                String strRejPath = rsnivel1.getString("directorio_rechazo");
                String strDMLInsert = rsnivel1.getString("dmlinsert");
                String strDMLDelete = rsnivel1.getString("dmldelete");
                String strDMLUpdate = rsnivel1.getString("dmlupdate");
                String strDMLInicial = rsnivel1.getString("dmlinicial");
                int intStartLine = rsnivel1.getInt("linea_inicial");
                int intOrdenacion = rsnivel1.getInt("ordenacion");
                String strTempPath = rsnivel1.getString("directorio_temporal");
                String strTipoEntrada = rsnivel1.getString("tipo_entrada");
                String strLecSufijo = rsnivel1.getString("extension_unzip");
                String strLecPrefijo = rsnivel1.getString("prefijo_unzip");
                String strTmpSufijo = rsnivel1.getString("extension_proceso");
                int intTransaccionMax = rsnivel1.getInt("bloque_transaccion");
                int intTransaccionCnt = 0;
                int intErroresMax = rsnivel1.getInt("limite_errores");
                int intErroresCnt = 0;
                strDMLError = rsnivel1.getString("dmlerror");
                //------------------------------------------------------------------
                //
                //------------------------------------------------------------------
                strDMLInsert = (strDMLInsert == null ? "" : strDMLInsert);
                strDMLDelete = (strDMLDelete == null ? "" : strDMLDelete);
                strDMLUpdate = (strDMLUpdate == null ? "" : strDMLUpdate);
                strDMLInicial = (strDMLInicial == null ? "" : strDMLInicial);
                boolean blnDMLInsert = (strDMLInsert.length() > 0);
                boolean blnDMLDelete = (strDMLDelete.length() > 0);
                boolean blnDMLUpdate = (strDMLUpdate.length() > 0);
                boolean blnDMLInicial = (strDMLInicial.length() > 0);
                strTmpSufijo = (strTmpSufijo == null ? "" : strTmpSufijo);
                strLecSufijo = (strLecSufijo == null ? "" : strLecSufijo);
                strLecPrefijo = (strLecPrefijo == null ? "" : strLecPrefijo);
                //--------------------------------------------------------------
                bufEmailBody.append(strDescripcion).append("\r\n");
                //--------------------------------------------------------------
                if (strTempPath != null) {
                    File file = new File(strTempPath);
                    if (!file.exists()) {
                        throw new Exception("El directorio '" + strTempPath + "' no existe o no se tiene acceso.");
                    }
                }
                //------------------------------------------------------------------
                // carga de toos los atributos de conexion, desencriptando aquellos que estan encriptados
                //------------------------------------------------------------------
                Map<String, String> vecConnData = subConnectionData(statement, resultset, intIdConexion);
                //------------------------------------------------------------------
                // proceso de importacion en funcion del protocolo configurado.
                //------------------------------------------------------------------
                if (!strProtocol.equals(CTE_PROTOCOL_SFTP) && !strProtocol.equals(CTE_PROTOCOL_FTP)) {
                    throw new Exception("No se reconoce el protocolo: " + strProtocol);
                }
                //------------------------------------------------------------------
                // Creo un array con el nomero de clunas a leer desde los archivos de entrada
                // si debe incrementarse el limite, solo debe modficarse el valor de CTE_FIELD_SIZE
                //------------------------------------------------------------------
                boolean[] blnHelperDelete = new boolean[CTE_FIELD_SIZE];
                boolean[] blnHelperInsert = new boolean[CTE_FIELD_SIZE];
                boolean[] blnHelperUpdate = new boolean[CTE_FIELD_SIZE];
                String[] strHelperField = new String[CTE_FIELD_SIZE];
                for (Integer j = 1, i = 0; i < strHelperField.length; i++, j++) {
                    strHelperField[i] = "{" + j.toString() + "}";
                    blnHelperInsert[i] = blnDMLInsert && strDMLInsert.contains(strHelperField[i]);
                    blnHelperDelete[i] = blnDMLDelete && strDMLDelete.contains(strHelperField[i]);
                    blnHelperUpdate[i] = blnDMLUpdate && strDMLUpdate.contains(strHelperField[i]);
                }
                //------------------------------------------------------------------
                // crear la conexion al repositorio de archivos
                //------------------------------------------------------------------
                try {
                    if (strProtocol.equals(CTE_PROTOCOL_FTP)) {
                        proxyftp = new ProxyFTPBean();
                        if (!proxyftp.connect(vecConnData.get("host"), vecConnData.get("username"), vecConnData.get("password"), Integer.parseInt(vecConnData.get("port")), strInPath)) {
                            throw new Exception("Error en la conexion al repositorio de archivos : " + proxyftp.getLastError());
                        }
                    } else {
                        proxyssh = new ProxySFTPBean();
                        if (!proxyssh.connect(vecConnData.get("host"), vecConnData.get("username"), vecConnData.get("password"), Integer.parseInt(vecConnData.get("port")), strInPath)) {
                            throw new Exception("Error en la conexion al repositorio de archivos : " + proxyssh.getLastError());
                        }
                    }
                } catch (Exception loc) {
                    throw new Exception(loc.getMessage());
                }
                //------------------------------------------------------------------
                // obtener la lista de los archivos del directorio actual
                //------------------------------------------------------------------
                String[] vecFiles = null;
                if (proxyftp != null) {
                    vecFiles = proxyftp.listFiles(strInPath, strInPrefijo, strInSufijo, intOrdenacion);
                    if (vecFiles == null) {
                        throw new Exception(proxyftp.getLastError());
                    }
                } else if (proxyssh != null) {
                    vecFiles = proxyssh.listFiles(strInPath, strInPrefijo, strInSufijo, intOrdenacion);
                    if (vecFiles == null) {
                        throw new Exception(proxyssh.getLastError());
                    }
                }
                //--------------------------------------------------------------------
                // preparar el dml para registrar los archivos cargados en la tabla tx_importacion
                //--------------------------------------------------------------------
                query = "insert into tx_importacion (sq_importacion,id_importacion,nombre_archivo,registros,fecha_inicio,fecha_final,nombre_paquete,estado)values(sec_importacion.nextval,?,?,?,?,?,?,?)";
                prepared = connection.prepareStatement(query);
                //--------------------------------------------------------------------
                // Loop para recorrer todos los archivos encontrados en el directorio actual
                //--------------------------------------------------------------------
                boolean blnRecordUpdated = false, blnFileSucceeded = false, blnRowWithoutError = false;
                java.sql.Timestamp sql1Timestamp = null, sql2Timestamp = null;
                int intRowsToLoad = 0, intRowsCounter = 0, intDmlResult = 0, intLinea = 0, intLineaInicial = 0;
                String strLine, strdelete, strinsert, strupdate, strrenamed;
                String strFullTmpFilename, strRemoteFilename = null;
                for (String strFile : vecFiles) {
                    System.out.println("IMPORTANDO:" + strFile);
                    //--------------------------------------------------------------------
                    // Traigo el archivo al repositorio temporal
                    //--------------------------------------------------------------------
                    if (proxyftp != null) {
                        proxyftp.changeDir(strInPath);
                    } else if (proxyssh != null) {
                        proxyssh.changeDir(strInPath);
                    }
                    List<String> listaFiles = new ArrayList<String>();
                    if (strTipoEntrada.equals("TXT")) {
                        boolean blnaux = false;
                        try {
                            if (proxyftp != null) {
                                blnaux = proxyftp.getFile(strFile, strTempPath);
                            } else if (proxyssh != null) {
                                blnaux = proxyssh.getFile(strFile, strTempPath);
                            }
                        } catch (Exception loc) {
                            continue;
                        }
                        if (blnaux) {
                            strRemoteFilename = changeFileExtension(strFile, strInSufijo, strTmpSufijo);
                            if (proxyftp != null) {
                                proxyftp.remFile(strRemoteFilename);
                                proxyftp.renFile(strFile, strRemoteFilename);
                            } else if (proxyssh != null) {
                                proxyssh.remFile(strRemoteFilename);
                                proxyssh.renFile(strFile, strRemoteFilename);
                            }
                            listaFiles.add(strFile);
                            strLecSufijo = strInSufijo;
                            strLecPrefijo = strInPrefijo;
                        } else {
                            continue;
                            /*
                             logger.error("No pudo trearse el archivo: " + strFile);
                             strrenamed = changeFileExtension(strFile, strInSufijo, strRejSufijo);
                             if (proxyftp != null) {
                             proxyftp.movFile(strFile, strRejPath, strrenamed);
                             } else if (proxyssh != null) {
                             proxyssh.movFile(strFile, strRejPath, strrenamed);
                             }*/
                        }
                    } else if (strTipoEntrada.equals("ZIP")) {
                        boolean blnaux = false;
                        if (proxyftp != null) {
                            blnaux = proxyftp.getFile(strFile, strTempPath);
                        } else if (proxyssh != null) {
                            blnaux = proxyssh.getFile(strFile, strTempPath);
                        }
                        if (blnaux) {
                            strFullTmpFilename = strTempPath + "/" + strFile;
                            if (unzip(strFullTmpFilename, strTempPath)) {
                                if (proxyftp != null) {
                                    proxyftp.changeDir(strOutPath);
                                    proxyftp.remFile(strFile);
                                    proxyftp.changeDir(strInPath);
                                    proxyftp.movFile(strFile, strOutPath, null);
                                } else if (proxyssh != null) {
                                    proxyssh.changeDir(strOutPath);
                                    proxyssh.remFile(strFile);
                                    proxyssh.changeDir(strInPath);
                                    proxyssh.movFile(strFile, strOutPath, null);
                                }
                                removeFile(strFullTmpFilename);
                                strRemoteFilename = null;
                                strFullTmpFilename = strTempPath + "/" + strLecPrefijo + "*" + strLecSufijo;
                                File file = new File(strTempPath);
                                String[] listaArchivos = file.list(new FileFilter(strLecPrefijo, strLecSufijo));
                                listaFiles = Arrays.asList(listaArchivos);
                            } else {
                                logger.log(Level.WARNING, "No pudo descomprimirse el archivo: {0}", strFile);
                                if (proxyftp != null) {
                                    proxyftp.movFile(strFile, strRejPath, null);
                                } else if (proxyssh != null) {
                                    proxyssh.movFile(strFile, strRejPath, null);
                                }
                            }
                        } else {
                            continue;
                            /*
                             logger.error("No pudo trearse el archivo: " + strFile);
                             if (proxyftp != null) {
                             proxyftp.movFile(strFile, strRejPath, null);
                             } else if (proxyssh != null) {
                             proxyssh.movFile(strFile, strRejPath, null);
                             }*/
                        }
                    } else {
                        throw new Exception("El tipo de archivo '" + strTipoEntrada + "' no es reconocido por el sistema.");
                    }
                    //--------------------------------------------------------------------
                    // Si no hay archivos a procesar, continuo
                    //--------------------------------------------------------------------
                    if (listaFiles.isEmpty()) {
                        continue;
                    }
                    //--------------------------------------------------------------------
                    // Loop de archivos a procesarse
                    //--------------------------------------------------------------------
                    for (String fichero : listaFiles) {
                        intFilesToLoad++;
                        //--------------------------------------------------------------------
                        // Ejecuto un DML inicial por cada archivo encontrado
                        //--------------------------------------------------------------------
                        if (blnDMLInicial) {
                            try {
                                if (strDMLInicial.length() > 0) {
                                    intDmlResult = statement.executeUpdate(strDMLInicial);
                                    connection.commit();
                                    Thread.sleep(500);
                                }
                            } catch (SQLException ex) {
                                connection.rollback();
                                throw new Exception("El DML inicial generó un error en la base de datos: " + ex.getMessage());
                                //logger.error("El DML inicial generó un error en la base de datos: " + ex.getMessage());
                                //continue;
                            }
                        }
                        //--------------------------------------------------------------------
                        // inicializacion de variables
                        //--------------------------------------------------------------------
                        sql1Timestamp = getCurrentTimeStamp();
                        strFullTmpFilename = strTempPath + "/" + fichero;
                        String[] vecFields;
                        intRowsToLoad = 0;
                        intRowsCounter = 0;
                        blnRecordUpdated = false;
                        intErroresCnt = 0;
                        intTransaccionCnt = 0;
                        intLinea = 0;
                        intLineaInicial = 1;
                        blnFileSucceeded = true;
                        //--------------------------------------------------------------------
                        // abro y consumo la cabecera de los archivos
                        //--------------------------------------------------------------------
                        FileReader filereader = new FileReader(strFullTmpFilename);
                        BufferedReader br = new BufferedReader(filereader);
                        if (intStartLine > 1) {
                            int c = 1;
                            while ((strLine = br.readLine()) != null) {
                                intLinea++;
                                if (++c >= intStartLine) {
                                    break;
                                }
                            }
                        }
                        //--------------------------------------------------------------------
                        // Inicio del loop de lectura del archivo
                        //--------------------------------------------------------------------
                        while ((strLine = br.readLine()) != null) {
                            intLinea++;
                            //--------------------------------------------------------------------
                            // lectura de linea del archivo y separo las columnas
                            //--------------------------------------------------------------------
                            if (strLine.equals("")) {
                                continue;
                            } else {
                                vecFields = strLine.split(strSeparator);
                            }
                            //--------------------------------------------------------------------
                            // loop de configuracion de dmls
                            //--------------------------------------------------------------------
                            blnRowWithoutError = true;
                            strdelete = strDMLDelete;
                            strinsert = strDMLInsert;
                            strupdate = strDMLUpdate;
                            for (int i = 0; i < strHelperField.length; i++) {
                                //--------------------------------------------------------------------
                                // contruyo el dml para hacer el update en la tabla del cliente
                                //--------------------------------------------------------------------
                                if (blnDMLUpdate && blnHelperUpdate[i]) {
                                    if (i < vecFields.length) {
                                        strupdate = strupdate.replace(strHelperField[i], vecFields[i]);
                                    } else {
                                        throw new Exception("No se encontró la columna " + i + " en la linea " + intLinea + " del archivo: '" + fichero + "'");
                                        //logger.error("No se encontró la columna " + i + " en la linea " + intLinea + " del archivo: '" + fichero + "'");
                                        //blnRowWithoutError = false;
                                        //break;
                                    }
                                }
                                //--------------------------------------------------------------------
                                // contruyo el dml para hacer delete en la tabla del cliente
                                //--------------------------------------------------------------------
                                if (blnDMLDelete && blnHelperDelete[i]) {
                                    if (i < vecFields.length) {
                                        strdelete = strdelete.replace(strHelperField[i], vecFields[i]);
                                    } else {
                                        throw new Exception("No se encontró la columna " + i + " en la linea " + intLinea + " del archivo: '" + fichero + "'");
                                        //logger.error("No se encontró la columna " + i + " en la linea " + intLinea + " del archivo: '" + fichero + "'");
                                        //blnRowWithoutError = false;
                                        //break;
                                    }
                                }
                                //--------------------------------------------------------------------
                                // contruyo el dml para hacer el insert en la tabla del cliente
                                //--------------------------------------------------------------------
                                if (blnDMLInsert && blnHelperInsert[i]) {
                                    if (i < vecFields.length) {
                                        strinsert = strinsert.replace(strHelperField[i], vecFields[i]);
                                    } else {
                                        throw new Exception("No se encontró la columna " + i + " en la linea " + intLinea + " del archivo: '" + fichero + "'");
                                        //logger.error("No se encontró la columna " + i + " en la linea " + intLinea + " del archivo: '" + fichero + "'");
                                        //blnRowWithoutError = false;
                                        //break;
                                    }
                                }
                            } //loop de columnas de linea
                            //--------------------------------------------------------------------
                            // Si ocurrio al error al crear lo DMLs , salir del ciclo de lectura de lineas del archivo
                            //--------------------------------------------------------------------
                            if (!blnRowWithoutError) {
                                blnFileSucceeded = false;
                                if (++intErroresCnt > intErroresMax) {
                                    break;
                                } else {
                                    continue;
                                }
                            }
                            //--------------------------------------------------------------------
                            // Si esta configurado para hacer un UPDATE de la deuda
                            //--------------------------------------------------------------------
                            if (blnDMLUpdate) {
                                try {
                                    //--------------------------------------------------------------------
                                    // Ejecuto el UPDATE
                                    //--------------------------------------------------------------------
                                    intDmlResult = statement.executeUpdate(strupdate);
                                    if (intDmlResult > 0) {
                                        blnRecordUpdated = true;
                                        intRowsCounter++;
                                    } else {
                                        blnRecordUpdated = false;
                                    }
                                } catch (SQLException ex) {
                                    throw new Exception("La linea '" + strLine + "' del archivo '" + fichero + "' generó un error al actualizar el registro: " + ex.getMessage());
                                    //logger.error("La linea '" + strLine + "' del archivo '" + fichero + "' generó un error al actualizar el registro: " + ex.getMessage());
                                    //blnFileSucceeded = false;
                                    //if (++intErroresCnt > intErroresMax) {
                                    //    break;
                                    //} else {
                                    //    continue;
                                    //}
                                }
                            }
                            //--------------------------------------------------------------------
                            // Si esta configurado para hacer un DELETE de deudas y el UPDATE no afecto filas o esta descativado
                            //--------------------------------------------------------------------
                            if (blnDMLDelete && !blnRecordUpdated) {
                                try {
                                    //--------------------------------------------------------------------
                                    // Ejecuto el DELETE
                                    //--------------------------------------------------------------------
                                    intDmlResult = statement.executeUpdate(strdelete);
                                } catch (SQLException ex) {
                                    throw new Exception("La linea '" + strLine + "' del archivo '" + fichero + "' generó un error al borrar el registro: " + ex.getMessage());
                                    //logger.error("La linea '" + strLine + "' del archivo '" + fichero + "' generó un error al borrar el registro: " + ex.getMessage());
                                    //blnFileSucceeded = false;
                                    //if (++intErroresCnt > intErroresMax) {
                                    //    break;
                                    //} else {
                                    //    continue;
                                    //}
                                }
                            }
                            //--------------------------------------------------------------------
                            // si esta configrado para hacer un INSERT de la deudas y el UPDATE no afecto filas o esta desactivado
                            //--------------------------------------------------------------------
                            if (blnDMLInsert && !blnRecordUpdated) {
                                try {
                                    //--------------------------------------------------------------------
                                    // Ejecuto el INSERT
                                    //--------------------------------------------------------------------
                                    intDmlResult = statement.executeUpdate(strinsert);
                                    intRowsCounter++;
                                } catch (SQLException ex) {
                                    throw new Exception("La linea '" + strLine + "' del archivo '" + fichero + "' generó un error al insertar el registro: " + ex.getMessage());
                                    //logger.error("La linea '" + strLine + "' del archivo '" + fichero + "' generó un error al insertar el registro: " + ex.getMessage());
                                    //blnFileSucceeded = false;
                                    //if (++intErroresCnt > intErroresMax) {
                                    //    break;
                                    //} else {
                                    //    continue;
                                    //}
                                }
                            }
                            //--------------------------------------------------------------------
                            // Evaluo el tamaño del bloque transaccional
                            //--------------------------------------------------------------------
                            if (++intTransaccionCnt >= intTransaccionMax) {
                                try {
                                    intTransaccionCnt = 0;
                                    intLineaInicial += intTransaccionMax;
                                    connection.commit();
                                    intRowsToLoad += intRowsCounter;
                                    intRowsCounter = 0;
                                    Thread.sleep(500);
                                } catch (SQLException sqlex) {
                                    connection.rollback();
                                    blnFileSucceeded = false;
                                    intRowsCounter = 0;
                                    int p = intLineaInicial;
                                    int q = intLineaInicial - intTransaccionMax;
                                    throw new Exception("Ocurrió una excepcion al consolidarla filas [" + p + "-" + q + "] " + sqlex.getMessage());
                                    //logger.fatal("Ocurrió una excepcion al consolidarla filas [" + p + "-" + q + "] " + sqlex.getMessage());
                                    //if (++intErroresCnt > intErroresMax) {
                                    //    break;
                                    //}
                                }
                            }
                        } //loop de lineas de archivo
                        br.close();
                        //--------------------------------------------------------------------
                        // commit si hay transaccion pendiente
                        //--------------------------------------------------------------------
                        if (intTransaccionCnt > 0) {
                            try {
                                connection.commit();
                                intRowsToLoad += intRowsCounter;
                            } catch (SQLException sqlex) {
                                connection.rollback();
                                throw new Exception("La consolidacion del archivo '" + fichero + "' falló: " + sqlex.getMessage());
                                //logger.error("La consolidacion del archivo '" + fichero + "' falló: " + sqlex.getMessage());
                                //blnFileSucceeded = false;
                            }
                        }
                        //--------------------------------------------------------------------
                        // inserto registro de archivo
                        //--------------------------------------------------------------------
                        sql2Timestamp = getCurrentTimeStamp();
                        prepared.setInt(1, intIdImportacion);
                        prepared.setString(2, fichero);
                        prepared.setInt(3, intRowsToLoad);
                        prepared.setTimestamp(4, sql1Timestamp);
                        prepared.setTimestamp(5, sql2Timestamp);
                        prepared.setString(6, strTipoEntrada.equals("ZIP") ? strFile : "");
                        prepared.setString(7, blnFileSucceeded ? "SUC" : "ERR");
                        prepared.execute();
                        connection.commit();
                        intTotalRowsLoaded += intRowsToLoad;
                        //--------------------------------------------------------------------
                        bufEmailBody.append("\t").append(fichero).append(blnFileSucceeded ? " OK\r\n" : " NOK\r\n");
                        //--------------------------------------------------------------------
                        // mover el archivo de entrada
                        //--------------------------------------------------------------------
                        if (blnFileSucceeded) {
                            //--------------------------------------------------------------------
                            // mover el archivo al directorio de salida
                            //--------------------------------------------------------------------
                            strrenamed = changeFileExtension(fichero, strLecSufijo, strOutSufijo);
                            if (strRemoteFilename == null) {
                                if (strOutPath != null && strOutPath.length() > 0) {
                                    if (proxyftp != null) {
                                        proxyftp.changeDir(strOutPath);
                                    } else if (proxyssh != null) {
                                        proxyssh.changeDir(strOutPath);
                                    }
                                }
                                if (proxyftp != null) {
                                    proxyftp.remFile(fichero);
                                    proxyftp.remFile(strrenamed);
                                    proxyftp.putFile(fichero, strTempPath);
                                    proxyftp.renFile(fichero, strrenamed);
                                } else if (proxyssh != null) {
                                    proxyssh.remFile(fichero);
                                    proxyssh.remFile(strrenamed);
                                    proxyssh.putFile(fichero, strTempPath);
                                    proxyssh.renFile(fichero, strrenamed);
                                }
                            } else {
                                if (strOutPath != null && strOutPath.length() > 0) {
                                    if (proxyftp != null) {
                                        proxyftp.changeDir(strOutPath);
                                        proxyftp.remFile(strrenamed);
                                        proxyftp.changeDir(strInPath);
                                        proxyftp.movFile(strRemoteFilename, strOutPath, strrenamed);
                                    } else if (proxyssh != null) {
                                        proxyssh.changeDir(strOutPath);
                                        proxyssh.remFile(strrenamed);
                                        proxyssh.changeDir(strInPath);
                                        proxyssh.movFile(strRemoteFilename, strOutPath, strrenamed);
                                    }
                                } else {
                                    if (proxyftp != null) {
                                        proxyftp.remFile(strrenamed);
                                        proxyftp.renFile(strRemoteFilename, strrenamed);
                                    } else if (proxyssh != null) {
                                        proxyssh.remFile(strrenamed);
                                        proxyssh.renFile(strRemoteFilename, strrenamed);
                                    }
                                }
                            }
                        } else {
                            //--------------------------------------------------------------------
                            // mover el archivo rechazado
                            //--------------------------------------------------------------------
                            strrenamed = changeFileExtension(fichero, strLecSufijo, strRejSufijo);
                            if (strRemoteFilename == null) {
                                if (strRejPath != null && strRejPath.length() > 0) {
                                    if (proxyftp != null) {
                                        proxyftp.changeDir(strRejPath);
                                    } else if (proxyssh != null) {
                                        proxyssh.changeDir(strRejPath);
                                    }
                                }
                                if (proxyftp != null) {
                                    proxyftp.putFile(fichero, strrenamed);
                                } else if (proxyssh != null) {
                                    proxyssh.putFile(fichero, strrenamed);
                                }
                            } else {
                                if (strRejPath != null && strRejPath.length() > 0) {
                                    if (proxyftp != null) {
                                        proxyftp.movFile(strRemoteFilename, strRejPath, strrenamed);
                                    } else if (proxyssh != null) {
                                        proxyssh.movFile(strRemoteFilename, strRejPath, strrenamed);
                                    }
                                } else {
                                    if (proxyftp != null) {
                                        proxyftp.renFile(strRemoteFilename, strrenamed);
                                    } else if (proxyssh != null) {
                                        proxyssh.renFile(strRemoteFilename, strrenamed);
                                    }
                                }
                            }
                            throw new Exception("El archivo '" + fichero + "' no fue procesado correctamente.");
                            //logger.warn("El archivo '" + fichero + "' no fue procesado correctamente.");
                        }
                        //--------------------------------------------------------------------
                        // elimino el archivo temporal
                        //--------------------------------------------------------------------
                        strFullTmpFilename = strTempPath + "/" + fichero;
                        removeFile(strFullTmpFilename);
                        intFilesLoaded++;
                    } // loop de la lista de archivos
                } //loop de paquetes de archivo o archivo de datos
            } // loop de configuracion de importacion
            rsnivel1.close();
            //--------------------------------------------------------------------
            // Editar la respuesta final del proceso
            //--------------------------------------------------------------------
            if (intFilesToLoad > 0) {
                if (intFilesLoaded == intFilesToLoad) {
                    result = "OK " + intTotalRowsLoaded + " filas procesadas de " + intFilesToLoad + " archivos.";
                } else {
                    result = "ATENCION " + intTotalRowsLoaded + " filas procesadas de " + intFilesLoaded + "/" + intFilesToLoad + " archivos.";
                    sendMail("Importacion " + strEmpresa + " concluida con errores.", bufEmailBody.toString(), lstEmailNot);
                }
            } else {
                result = "OK";
            }
            //------------------------------------------------------------------
            // desbloqueo los pagos
            //------------------------------------------------------------------
            //statement.execute("update di_pagos set estado='ON' where cod_servicio='"+strout+"'");
            //connection.commit();
            //--------------------------------------------------------------------
            // cierro db
            //--------------------------------------------------------------------
            connection.close();
        } catch (NumberFormatException ex) {
            blnErrorOcurred = true;
            result = CTE_ERROR_LABEL2;
            logger.severe(ex.getMessage());
            sendMail("Importacion " + strEmpresa + " abortada por error en formato.", bufEmailBody.toString() + ex.getMessage(), lstEmailErr);
        } catch (SQLException ex) {
            blnErrorOcurred = true;
            result = CTE_ERROR_LABEL2;
            logger.severe(ex.getMessage());
            sendMail("Importacion " + strEmpresa + " abortada por error en base de datos.", bufEmailBody.toString() + ex.getMessage(), lstEmailErr);
        } catch (NamingException ex) {
            blnErrorOcurred = true;
            result = CTE_ERROR_LABEL2;
            logger.severe(ex.getMessage());
            sendMail("Importacion " + strEmpresa + " abortada por error en contexto.", bufEmailBody.toString() + ex.getMessage(), lstEmailErr);
        } catch (Exception ex) {
            blnErrorOcurred = true;
            result = CTE_ERROR_LABEL1 + ex.getMessage();
            logger.severe(ex.getMessage());
            sendMail("Importacion " + strEmpresa + " abortada por error en el proceso.", bufEmailBody.toString() + ex.getMessage(), lstEmailErr);
        } finally {
            //--------------------------------------------------------------------
            // Desconexion de los repositorios
            //--------------------------------------------------------------------
            if (proxyftp != null) {
                proxyftp.disconnect();
            }
            if (proxyssh != null) {
                proxyssh.disconnect();
            }
            //--------------------------------------------------------------------
            // Desconexion JDBC
            //--------------------------------------------------------------------
            if (connection != null) {
                try {
                    if (!connection.isClosed()) {
                        connection.rollback();
                    }
                } catch (SQLException ex) {
                    java.util.logging.Logger.getLogger(ImportBean.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                }
                try {
                    if (prepared != null) {
                        if (!prepared.isClosed()) {
                            prepared.close();
                        }
                    }
                    if (stmex != null) {
                        if (rsnivel1 != null) {
                            if (!rsnivel1.isClosed()) {
                                rsnivel1.close();
                            }
                        }
                        if (!stmex.isClosed()) {
                            stmex.close();
                        }
                    }
                    if (statement != null) {
                        if (resultset != null) {
                            if (!resultset.isClosed()) {
                                resultset.close();
                                //logger.debug("Registro cerrado.");
                            }
                        }
                        //--------------------------------------------------------------
                        // Bloque en caso de algun error ocurrido
                        //--------------------------------------------------------------
                        if (blnErrorOcurred && strDMLError != null) {
                            //statement.execute("update di_pagos set estado='ON' where cod_servicio='"+strout+"'");
                            //connection.commit();
                            statement.execute(strDMLError);
                            connection.commit();
                            System.out.println("EJECUTO DML DE EMERGENCIA!");
                        }
                        //--------------------------------------------------------------                        
                        if (!statement.isClosed()) {
                            statement.close();
                        }
                    }
                    if (!connection.isClosed()) {
                        connection.close();
                    }
                } catch (SQLException ex) {
                    java.util.logging.Logger.getLogger(ImportBean.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                }
            }
            logger.log(Level.INFO, "TERMINA IMPORTACION {0}", strEmpresa);
        }
        return result;
    }

    /**
     * *************************************************************************
     * Establece conexion al datasource llamado "unico_collector_ds" establecido
     * en el AS
     * @return Retorna un objeto conexion abierto.
     * @throws NamingException
     * @throws SQLException
     * ************************************************************************
     */
    private Connection getConnection() throws NamingException, SQLException {
        Context ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup("jdbc/unico_collector_ds");
        Connection con = ds.getConnection();
        con.setAutoCommit(false);
        return con;
    }

    /**
     * *************************************************************************
     * Encripta expresiones
     * @param strValue Expresion a encriptar
     * @return Cadena encriptada
     * ************************************************************************
     */
    @Override
    public String encrypt(String strValue) {
        String result = null;
        try {
            CryptoBean crypto = new CryptoBean();
            result = crypto.encrypt(strValue, CTE_COMPONENT_KEY);
        } catch (Exception ex) {
            result = "ERROR: " + ex.getMessage();
        }
        return result;
    }

    /**
     * *************************************************************************
     * Funcion simple que retorna la hora actual en formato Sql.Timestamp
     * @return Objeto Sql.Timestamp con la hora actual del SA
     * ************************************************************************
     */
    private static java.sql.Timestamp getCurrentTimeStamp() {
        java.util.Date today = new java.util.Date();
        return new java.sql.Timestamp(today.getTime());
    }

}
