package com.mc4.unico.blogic;

import javax.ejb.Remote;

@Remote
public interface ImportBeanRemote  {
    public String collect(String strEmpresa, String strServicio);
    public String export(String strEmpresa, String strServicio);
    public String encrypt(String strValue);
}
