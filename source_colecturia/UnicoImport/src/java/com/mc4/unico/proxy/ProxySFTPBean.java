
package com.mc4.unico.proxy;

import com.jcraft.jsch.*;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;
import java.util.logging.Level;

/**
 * *****************************************************************************
 * Componente para hacer la importacion de archivo de datos
 * @author fernando flores
 * @version 1.0.0.16072014
 * ****************************************************************************
 */
public class ProxySFTPBean  {

    private JSch ssh = null;
    private Session session = null;
    private Channel channel = null;
    private ChannelSftp sftp = null;
    private String strErrorDescription = "";
    BufferedReader reader = null;

    /**
     * Contructor
     */
    public ProxySFTPBean() {
        ssh = new JSch();
        JSch.setConfig("StrictHostKeyChecking", "no");
    }

    /**
     * Establece la conexion SFTP, opcionalmente establece el directorio actual
     *
     * @param host IP del servidor SFTP
     * @param username nombre de usuario
     * @param password contraseña del usuario
     * @param port numero de puerto
     * @param directory ruta del directorio actual
     * @return Retorna TRUE si la conexion se ha establecida, FALSE de lo
     * contrario e indica consultar el metodo getLastError
     */
    public boolean connect(String host, String username, String password, Integer port, String directory) {
        boolean result = false;

        strErrorDescription = "";
        try {
            //------------------------------------------------------------------
            // Establezco la conexion
            //------------------------------------------------------------------
            session = ssh.getSession(username, host, port);
            session.setPassword(password);
            session.connect();
            channel = session.openChannel("sftp");
            channel.connect();
            if (channel.isConnected()) {
                sftp = (ChannelSftp) channel;
                //------------------------------------------------------------------
                // Establezco la ruta actual remoto
                //------------------------------------------------------------------
                if (directory != null && directory.length() > 0) {
                    sftp.cd(directory);
                }
                result = true;
            }
        } catch (JSchException ex) {
            strErrorDescription = ex.getMessage();
        } catch (SftpException ex) {
            strErrorDescription = ex.getMessage();
        }
        return result;
    }

    /**
     * Verifica si la conexion esta establecida, si es asi la desconecta.
     */
    public void disconnect() {
        strErrorDescription = "";
        try {
            if (sftp.isConnected()) {
                sftp.disconnect();
            }
            if (channel.isConnected()) {
                channel.disconnect();
            }
            if (session.isConnected()) {
                session.disconnect();
            }
        } catch (Exception ex) {
            strErrorDescription = ex.getMessage();
        } finally {
            session = null;
        }
    }

    /**
     * Importa un archivo al directorio origen
     * @param strSrcFilename nombre de archivo
     * @param strDstDirectory directorio local donde sera copiado
     * @return Retorn TRUE en caso de exito.
     */
    public boolean getFile(String strSrcFilename, String strDstDirectory) {
        boolean result = false;

        try {
            sftp.get(strSrcFilename, strDstDirectory);
            result = true;
        } catch (SftpException ftpex) {
            java.util.logging.Logger.getLogger(ProxySFTPBean.class.getName()).log(Level.SEVERE, null, ftpex);
        }
        return result;
    }

    public boolean remFile(String strSrcFilename) {
        boolean result = false;
        try {
            sftp.rm(strSrcFilename);
            result = true;
        } catch (SftpException ftpex) {
            result = false;
        }
        return result;
    }

    /**
     * Exporta un archivo al directorio destino.
     * @param strSrcFilename Nombre del archivo local origen.
     * @param strSrcDirectory Directorio donde se encuentra el archivo
     * @return Retorn TRUE en caso de exito.
     */
    public boolean putFile(String strSrcFilename, String strSrcDirectory) {
        boolean result = false;
        String strFullFilename;

        try {
            if (strSrcDirectory != null) {
                strFullFilename = strSrcDirectory + "/" + strSrcFilename;
            } else {
                strFullFilename = strSrcFilename;
            }
            sftp.put(strFullFilename, strSrcFilename);
            result = true;
        } catch (SftpException ftpex) {
            java.util.logging.Logger.getLogger(ProxySFTPBean.class.getName()).log(Level.SEVERE, null, ftpex);
        }
        return result;
    }

    /**
     * Metodo para obtener los archivos de un directorio remoto.
     *
     * @param strDirectory Ruta del directorio del cual se desea la lista de
     * archivos.
     * @param strPrefijo Subcadena inicila de los archivos.
     * @param strExtension Filtro por extension de los archivos.
     * @param intOrdenacion Indica si se debe aplicar una ordenacion a los
     * archivos (0=ninguno,1=acendente normal,2=acendente inverso)
     * @return Retorna un arreglo de los nombres de los archivos encontrados,
     * NULL en caso de error.
     */
    public String[] listFiles(String strDirectory, String strPrefijo, String strExtension, int intOrdenacion) {
        String[] result = null;
        int k = 0;
        ArrayList<String> lstFileList = new ArrayList<String>();
        String strFilename;
        Vector vector;

        try {
            strErrorDescription = "";
            //------------------------------------------------------------------
            // Obtengo la lista de archivos del directorio remoto actual, 
            // equivalente a un comando LS
            //------------------------------------------------------------------
            if (strPrefijo == null) {
                if (strExtension == null) {
                    vector = sftp.ls("./*");
                } else {
                    vector = sftp.ls("./*" + strExtension);
                }
            } else {
                if (strExtension == null) {
                    vector = sftp.ls("./" + strPrefijo + "*");
                } else {
                    vector = sftp.ls("./" + strPrefijo + "*" + strExtension);
                }
            }
            //------------------------------------------------------------------
            // Elimino detalles de la lista, dejando solo los nombres de archivo
            //------------------------------------------------------------------
            for (Object vector1 : vector) {
                strFilename = vector1.toString();
                k = strFilename.lastIndexOf(" ");
                if (k > 0) {
                    strFilename = strFilename.substring(k).trim();
                    lstFileList.add(strFilename);
                }
            }
            //------------------------------------------------------------------
            // Ordenacion de la lista de nombres de archivos
            //------------------------------------------------------------------
            switch (intOrdenacion) {
                case 1:
                    Collections.sort(lstFileList);
                    break;
                case 2:
                    StringBuilder buffer = new StringBuilder();
                    for (int i = 0; i < lstFileList.size(); i++) {
                        buffer.append(lstFileList.get(i));
                        lstFileList.set(i, buffer.reverse().toString());
                        buffer.setLength(0);
                    }
                    Collections.sort(lstFileList);
                    for (int i = 0; i < lstFileList.size(); i++) {
                        buffer.append(lstFileList.get(i));
                        lstFileList.set(i, buffer.reverse().toString());
                        buffer.setLength(0);
                    }
                    break;
            }
            //------------------------------------------------------------------
            // Retorno la lista de archivos remotos
            //------------------------------------------------------------------
            result = lstFileList.toArray(new String[lstFileList.size()]);
        } catch (SftpException ex) {
            strErrorDescription = ex.getMessage();
        }
        return result;
    }

    /**
     * Metod para recuperar la descripcion del ultimo error ocurrido.
     *
     * @return Retorna la descripcion del error.
     */
    public String getLastError() {
        return strErrorDescription;
    }

    public boolean changeDir(String strDstDirectory) {
        boolean result = false;

        strErrorDescription = "";
        try {
            sftp.cd(strDstDirectory);
            result = true;
        } catch (SftpException ex) {
            strErrorDescription = ex.getMessage();
        }
        return result;
    }

    /**
     * Metodo para mover un archivo del directorio actual a un directorio
     * destino.
     * @param strSrcFilename nombre del archivo local
     * @param strDstDirectory directorio destino
     * @param strDstFilename nombre destino al que sera renombra
     * @return Retorna TRUE en caso de exito, FALSE de lo contrario.
     */
    public boolean movFile(String strSrcFilename, String strDstDirectory, String strDstFilename) {
        boolean result = false;

        strErrorDescription = "";
        try {
            if (strDstFilename != null) {
                sftp.rename(strSrcFilename, strDstDirectory + "/" + strDstFilename);
            } else {
                sftp.rename(strSrcFilename, strDstDirectory + "/" + strSrcFilename);
            }
            result = true;
        } catch (SftpException ex) {
            strErrorDescription = ex.getMessage();
        }
        return result;
    }

    public boolean renFile(String strSrcFilename, String strDstFilename) {
        boolean result = false;

        try {
            strErrorDescription = "";
            sftp.rename(strSrcFilename, strDstFilename);
            result = true;
        } catch (SftpException ex) {
            java.util.logging.Logger.getLogger(ProxySFTPBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

}
