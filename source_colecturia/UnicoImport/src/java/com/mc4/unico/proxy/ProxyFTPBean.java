
package com.mc4.unico.proxy;

import it.sauronsoftware.ftp4j.FTPAbortedException;
import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferException;
import it.sauronsoftware.ftp4j.FTPException;
import it.sauronsoftware.ftp4j.FTPFile;
import it.sauronsoftware.ftp4j.FTPIllegalReplyException;
import it.sauronsoftware.ftp4j.FTPListParseException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Clase conector FTP
 * @author fernando flores
 * @version 1.0.0.02082014
 */
public class ProxyFTPBean {

    private FTPClient ftp = null;
    private String strErrorDesc = "";

    /**
     * Constructor
     */
    public ProxyFTPBean() {
        ftp = new FTPClient();
    }

    /**
     * Metodo que hace la conexion
     * @param host host
     * @param username usuario
     * @param password contraseña
     * @param port puerto
     * @param directory directorio incial (opcional)
     * @return true en caso de exito
     */
    public boolean connect(String host, String username, String password, Integer port, String directory) {
        boolean result = false;
        try {
            strErrorDesc = "";
            ftp.connect(host, port);
            ftp.login(username, password);
            if (ftp.isConnected()) {
                if (directory != null && directory.length() > 0) {
                    ftp.changeDirectory(directory);
                }
                result = true;
            }
        } catch (IllegalStateException ex) {
            strErrorDesc = ex.getMessage();
        } catch (IOException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPIllegalReplyException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPException ex) {
            strErrorDesc = ex.getMessage();
        }
        return result;
    }

    /**
     * Ejecuta la desconexion
     */
    public void disconnect() {
        try {
            strErrorDesc = "";
            if (ftp != null) {
                if (ftp.isConnected()) {
                    ftp.disconnect(true);
                }
            }
        } catch (IllegalStateException ex) {
            strErrorDesc = ex.getMessage();
        } catch (IOException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPIllegalReplyException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPException ex) {
            strErrorDesc = ex.getMessage();
        }
    }

    public boolean changeDir(String strDstDirectory) {
        boolean result = false;

        try {
            strErrorDesc = "";
            ftp.changeDirectory(strDstDirectory);
            result = true;
        } catch (IllegalStateException ex) {
            strErrorDesc = ex.getMessage();
        } catch (IOException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPIllegalReplyException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPException ex) {
            strErrorDesc = ex.getMessage();
        }
        return result;
    }

    public boolean renFile(String strSrcFilename, String strDstFilename) {
        boolean result = false;

        try {
            strErrorDesc = "";
            ftp.rename(strSrcFilename, strDstFilename);
            result = true;
        } catch (IllegalStateException ex) {
            strErrorDesc = ex.getMessage();
        } catch (IOException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPIllegalReplyException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPException ex) {
            strErrorDesc = ex.getMessage();
        }
        return result;
    }

    public boolean movFile(String strSrcFilename, String strDstDirectory, String strDstFilename) {
        boolean result = false;

        try {
            strErrorDesc = "";
            if (strDstFilename != null) {
                ftp.rename(strSrcFilename, strDstDirectory + "/" + strDstFilename);
            } else {
                ftp.rename(strSrcFilename, strDstDirectory + "/" + strSrcFilename);
            }
            result = true;
        } catch (IllegalStateException ex) {
            strErrorDesc = ex.getMessage();
        } catch (IOException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPIllegalReplyException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPException ex) {
            strErrorDesc = ex.getMessage();
        }
        return result;
    }

    public boolean getFile(String strSrcFilename, String strDstDirectory) {
        boolean result = false;
        String strFullFilename = strDstDirectory + "/" + strSrcFilename;

        try {
            strErrorDesc = "";
            File fichero = new File(strFullFilename);
            if (fichero.exists()) {
                fichero.delete();
            }
            ftp.download(strSrcFilename, fichero);
            result = true;
        } catch (IllegalStateException ex) {
            strErrorDesc = ex.getMessage();
        } catch (IOException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPIllegalReplyException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPDataTransferException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPAbortedException ex) {
            strErrorDesc = ex.getMessage();
        }
        return result;
    }

    public boolean putFile(String strSrcFilename, String strSrcDirectory) {
        boolean result = false;
        String strFullFilename;

        try {
            strErrorDesc = "";
            if (strSrcDirectory != null) {
                strFullFilename = strSrcDirectory + "/" + strSrcFilename;
            } else {
                strFullFilename = strSrcFilename;
            }
            File fichero = new File(strFullFilename);
            if (!fichero.exists()) {
                throw new Exception("El archivo '" + strFullFilename + "' no existe.");
            }
            ftp.upload(fichero);
            result = true;
        } catch (IllegalStateException ex) {
            strErrorDesc = ex.getMessage();
        } catch (IOException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPIllegalReplyException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPDataTransferException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPAbortedException ex) {
            strErrorDesc = ex.getMessage();
        } catch (Exception ex) {
            strErrorDesc = ex.getMessage();
        }
        return result;
    }

    public boolean remFile(String strSrcFilename) {
        boolean result = false;

        try {
            strErrorDesc = "";
            ftp.deleteFile(strSrcFilename);
            result = true;
        } catch (IllegalStateException ex) {
            strErrorDesc = ex.getMessage();
        } catch (IOException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPIllegalReplyException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPException ex) {
            strErrorDesc = ex.getMessage();
        }
        return result;
    }

    /**
     * Lista los archivos de un directorio
     * @param strDirectory ruta del directorio
     * @param strPrefijo prefijo filtro de los nombres de los archivos
     * @param strExtension extension filtro de los nombres de los archivos
     * @param intOrdenacion tipo de ordenacion por nombre de archivo 1=nombre de izquerda a derecha, 2=nombre de derecha a izquierda
     * @return arreglo con los nombres de archivo
     */
    public String[] listFiles(String strDirectory, String strPrefijo, String strExtension, int intOrdenacion) {
        String[] result = null;
        String strpath = "";
        ArrayList<String> lstFileList = new ArrayList<String>();

        try {
            strErrorDesc = "";
            //------------------------------------------------------------------
            // contruyo el filtro
            //------------------------------------------------------------------
            if (strPrefijo == null) {
                if (strExtension == null) {
                    strpath = "./*";
                } else {
                    strpath = "./*" + strExtension;
                }
            } else {
                if (strExtension == null) {
                    strpath = "./" + strPrefijo + "*";
                } else {
                    strpath = "./" + strPrefijo + "*" + strExtension;
                }
            }
            //------------------------------------------------------------------
            // obtentgo el listado de archivos
            //------------------------------------------------------------------
            FTPFile[] list = ftp.list(strpath);
            for (FTPFile file : list) {
                lstFileList.add(file.getName());
            }
            //------------------------------------------------------------------
            // Ordenacion de la lista de nombres de archivos
            //------------------------------------------------------------------
            switch (intOrdenacion) {
                case 1:
                    Collections.sort(lstFileList);
                    break;
                case 2:
                    StringBuilder buffer = new StringBuilder();
                    for (int i = 0; i < lstFileList.size(); i++) {
                        buffer.append(lstFileList.get(i));
                        lstFileList.set(i, buffer.reverse().toString());
                        buffer.setLength(0);
                    }
                    Collections.sort(lstFileList);
                    for (int i = 0; i < lstFileList.size(); i++) {
                        buffer.append(lstFileList.get(i));
                        lstFileList.set(i, buffer.reverse().toString());
                        buffer.setLength(0);
                    }
                    break;
            }
            //------------------------------------------------------------------
            // inserto los resultados
            //------------------------------------------------------------------
            result = lstFileList.toArray(new String[lstFileList.size()]);
        } catch (IllegalStateException ex) {
            strErrorDesc = ex.getMessage();
        } catch (IOException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPIllegalReplyException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPDataTransferException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPAbortedException ex) {
            strErrorDesc = ex.getMessage();
        } catch (FTPListParseException ex) {
            strErrorDesc = ex.getMessage();
        }
        return result;
    }

    public String getLastError() {
        return strErrorDesc;
    }

}
