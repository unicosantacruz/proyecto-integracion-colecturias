
package com.mc4.unico.proxy;

import java.io.File;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.ejb.Stateless;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Clase para enviar emails
 * @author fernando flores
 * @version 1.0.0.02082014
 */
public class ProxySMTPBean { 

    /**
     * Crea una instancia del recurso javamail 
     * @return sesion javamail
     */
    private Session lookupMailSession() {
        Session session = null;
        try {
            Context c = new InitialContext();
            session = (Session) c.lookup("mail/unico_collector_ms");
        } catch (NamingException ne) {
            session = null;
        }
        return session;
    }

    /**
     * Metodo que envia el mensaje
     * @param strSubject asunto
     * @param strBody cuerpo del mensaje
     * @param vecRecipient lista de destinatarios
     * @return retorna "OK" o la descripcion del error
     */
    public String sendMail(String strSubject, String strBody, String[] vecRecipient) {
        Session session = lookupMailSession();
        if (session == null) {
            return "No se encuentró la sesion javamail en el servidor de aplicación.";
        }
        String strHost = session.getProperty("mail.smtp.host");
        String strPort = session.getProperty("mail.smtp.port");
        String strUser = session.getProperty("mail.smtp.user");
        String strPass = session.getProperty("mail.smtp.password");
        String strFrom = session.getProperty("mail.smtp.user");
        String strName = session.getProperty("mail.smtp.name");
        String strResult = sendMail(strHost, strPort, strUser, strPass, strBody, null, strSubject, strName, strFrom, vecRecipient, false);
        return strResult;
    }


    public String sendMail(String strHost, String strPort, String strUser, String strPassword, String strBody, String[] vecAtachment, String strSubject, String strFromName, String strFrom, String[] vecRecipient, boolean blnDebug) {
        String result = "";

        try {
            //----------------------------------------------------------------------
            // Verifico si los archivos adjuntos existen
            //----------------------------------------------------------------------
            if (vecAtachment != null) {
                File file = null;
                for (String adjunto : vecAtachment) {
                    file = new File(adjunto);
                    if (!file.exists()) {
                        throw new Exception("El archivo adjunto '" + adjunto + "' no existe.");
                    }
                }
            }
            //----------------------------------------------------------------------
            // Establecer propiedades para del email
            //----------------------------------------------------------------------
            Properties props = new Properties();
            props.setProperty("mail.smtp.host", strHost);  //"smtp.gmail.com" mail.mc4.com.bo //"mail.mc4.com.bo"
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", strPort);  //"587" 110-25 //"25"
            props.setProperty("mail.smtp.user", strUser);  //"fflores@mc4.com.bo"
            props.setProperty("mail.smtp.auth", "true");
            Session session = Session.getDefaultInstance(props);
            session.setDebug(blnDebug);
            //----------------------------------------------------------------------
            // Contruccion del cuerpo del mensaje
            //----------------------------------------------------------------------
            MimeMultipart multiparte = new MimeMultipart();
            BodyPart texto = new MimeBodyPart();
            texto.setText(strBody);
            multiparte.addBodyPart(texto);
            if (vecAtachment != null) {
                int k = 0;
                String filename = "";
                for (String filepath : vecAtachment) {
                    k = filepath.lastIndexOf("/");
                    if (k < 0) {
                        filename = filepath.substring(k);
                    } else {
                        filename = filepath;
                    }
                    BodyPart adjunto = new MimeBodyPart();
                    adjunto.setDataHandler(new DataHandler(new FileDataSource(filepath)));
                    adjunto.setFileName(filename);
                    multiparte.addBodyPart(adjunto);
                }
            }
            //----------------------------------------------------------------------
            //
            //----------------------------------------------------------------------
            MimeMessage message = new MimeMessage(session);
            message.setSubject(strSubject);
            message.setFrom(new InternetAddress(strFrom, strFromName));  //"fflores@mc4.com.bo"
            message.setContent(multiparte);
            //----------------------------------------------------------------------
            // envio del mensaje
            //----------------------------------------------------------------------
            Transport t = session.getTransport("smtp");
            t.connect(strUser, strPassword);  //"fflores@mc4.com.bo"
            for (String destino : vecRecipient) {
                message.setRecipient(Message.RecipientType.TO, new InternetAddress(destino));
                try {
                    t.sendMessage(message, message.getAllRecipients());
                } catch (MessagingException ex) {
                    result += destino + ",";
                }
            }
            t.close();
            //----------------------------------------------------------------------
            // resultado
            //----------------------------------------------------------------------
            if (result.equals("")) {
                result = "OK";
            } else {
                result = "Falla en el envio a estas direcciones: " + result.substring(0, result.length()-1);
            }
        } catch (MessagingException ex) {
            result = ex.getMessage();
        } catch (Exception ex) {
            result = ex.getMessage();
        }
        return result;
    }


    public String recvMail() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
