/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mc4.unico.faces;

import com.mc4.unico.blogic.ColecturiaBeanRemote;
import com.mc4.unico.obj.ListaIdentificadores;
import com.mc4.unico.obj.ListaPagoPeriodo;
import com.mc4.unico.obj.ListaParametros;
import com.mc4.unico.obj.ResultadoDeudas;
import com.mc4.unico.obj.ResultadoPagos;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author fer
 */
@WebService(serviceName = "ColecturiasWs")
@Stateless()
public class ColecturiasWs {
    @EJB
    private ColecturiaBeanRemote ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "getDeudas")
    public ResultadoDeudas getDeudas(@WebParam(name = "compania") String compania, @WebParam(name = "servicio") String servicio, @WebParam(name = "claves") ListaIdentificadores claves) {
        return ejbRef.getDeudas(compania, servicio, claves);
    }

    @WebMethod(operationName = "payDeudas")
    public ResultadoPagos payDeudas(@WebParam(name = "monto") Double monto, @WebParam(name = "periodos") ListaPagoPeriodo periodos, @WebParam(name = "compania") String compania, @WebParam(name = "servicio") String servicio, @WebParam(name = "claves") ListaIdentificadores claves, @WebParam(name = "telefono") String telefono, @WebParam(name = "PTM") String PTM, @WebParam(name = "extra") String extra) {
        return ejbRef.payDeudas(monto, periodos, compania, servicio, claves, telefono, PTM, extra);
    }

    @WebMethod(operationName = "getPrmGetDeudas")
    public ListaParametros getPrmGetDeudas(@WebParam(name = "compania") String compania, @WebParam(name = "servicio") String servicio) {
        return ejbRef.getPrmGetDeudas(compania, servicio);
    }

    @WebMethod(operationName = "getPrmPayDeudas")
    public ListaParametros getPrmPayDeudas(@WebParam(name = "compania") String compania, @WebParam(name = "servicio") String servicio) {
        return ejbRef.getPrmPayDeudas(compania, servicio);
    }
    
}
