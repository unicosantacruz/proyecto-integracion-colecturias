/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mc4.unico.types;

import java.util.Calendar;

/**
 *
 * @author fer
 */
public class TipoCambioActual {
    private double dblCambio;
    private int intFecha;

    public TipoCambioActual() {
        dblCambio = 0d;
        intFecha = 0;
    }
    
    public double getCambio(){
        if (intFecha==0){
            return 0d;
        }else{
            Calendar calActual = Calendar.getInstance();
            if (calActual.get(Calendar.DAY_OF_MONTH)==intFecha){
                return dblCambio;
            }else{
                return 0d;
            }
        }
    }
    
    public void setCambio(double dblCambio){
        this.intFecha = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        this.dblCambio = dblCambio;
    }
}
