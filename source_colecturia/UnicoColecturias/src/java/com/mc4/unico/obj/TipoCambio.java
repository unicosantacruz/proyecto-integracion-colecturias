
package com.mc4.unico.obj;

import java.io.Serializable;

/**
 *
 * @author fer
 */
public class TipoCambio implements Serializable {
    public String moneda;
    public Double cambio;

    public TipoCambio(String moneda, Double cambio) {
        this.moneda = moneda;
        this.cambio = cambio;
    }
}
