/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc4.unico.obj;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author fer
 */
public class CachePagos {

    private String strSelectDeuda;
    private String strSelectNombre;
    private String strFunPagoTotal;
    private String strFunPagoParcial;
    private String strFunPagoAdelantado;
    private String strFunPagoPeriodo;
    private char chrTipoPago;
    private int intIdPago;
    private double dblPagoMinimo;
    private double dblPagoMaximo;
    private boolean blnPagoLimitado;
    private HashMap<String, Integer> mapDatosExtra;

    public CachePagos(int intIdPago, String strSelectDeuda, String strFunPagoTotal, String strFunPagoParcial, String strFunPagoAdelantado, String strFunPagoPeriodo, String strSelectNombre, int intPagoMinimo, int intPagoMaximo, char chrTipoPago, String strDato1, String strDato2, String strDato3, String strDato4, String strDato5) {
        this.intIdPago = intIdPago;
        this.strSelectDeuda = strSelectDeuda;
        this.strFunPagoTotal = strFunPagoTotal;
        this.strFunPagoParcial = strFunPagoParcial;
        this.strFunPagoAdelantado = strFunPagoAdelantado;
        this.strFunPagoPeriodo = strFunPagoPeriodo;
        this.strSelectNombre = strSelectNombre;
        this.chrTipoPago = chrTipoPago;
        this.dblPagoMinimo = intPagoMinimo;
        this.dblPagoMaximo = intPagoMaximo;
        this.blnPagoLimitado = (intPagoMinimo > 0 || intPagoMaximo > 0);
        mapDatosExtra = new HashMap<String, Integer>();
        if (strDato1 != null && strDato1.length() > 0) {
            mapDatosExtra.put(strDato1, 1);
        }
        if (strDato2 != null && strDato2.length() > 0) {
            mapDatosExtra.put(strDato2, 2);
        }
        if (strDato3 != null && strDato3.length() > 0) {
            mapDatosExtra.put(strDato3, 3);
        }
        if (strDato4 != null && strDato4.length() > 0) {
            mapDatosExtra.put(strDato4, 4);
        }
        if (strDato5 != null && strDato5.length() > 0) {
            mapDatosExtra.put(strDato5, 5);
        }
    }
    
    public String[] getDatosExtra(){
        if (mapDatosExtra.size()>0){
            return mapDatosExtra.keySet().toArray(new String[mapDatosExtra.size()]);
        }else{
            return null;
        }
    }
    
    public int getDatoExtra(String nombre){
        if (mapDatosExtra.get(nombre)==null){
            return 0;
        }else{
            return mapDatosExtra.get(nombre);
        }
    }

    public boolean isPagoLimitado() {
        return blnPagoLimitado;
    }

    public double getPagoMinimo() {
        return dblPagoMinimo;
    }

    public double getPagoMaximo() {
        return dblPagoMaximo;
    }

    public int getIdPago() {
        return this.intIdPago;
    }

    public String getSelectDeuda() {
        return strSelectDeuda;
    }

    public String getSelectNombre() {
        return strSelectNombre;
    }

    public String getFuncionPago() {
        switch (this.chrTipoPago) {
            case 'T':
                return this.strFunPagoTotal;
            case 'P':
                return this.strFunPagoParcial;
            case 'A':
                return this.strFunPagoAdelantado;
            default:
                return null;
        }
    }

    public char getTipoPago() {
        return chrTipoPago;
    }

    public String getTipoPagoDescripcion() {
        switch (this.chrTipoPago) {
            case 'T':
                return "TOTAL";
            case 'P':
                return "PARCIAL";
            case 'A':
                return "ADELANTADO";
            default:
                return null;
        }
    }
}
