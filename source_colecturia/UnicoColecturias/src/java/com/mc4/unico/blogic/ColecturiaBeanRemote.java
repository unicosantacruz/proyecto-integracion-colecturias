
package com.mc4.unico.blogic;

import com.mc4.unico.obj.ListaIdentificadores;
import com.mc4.unico.obj.ListaPagoPeriodo;
import com.mc4.unico.obj.ListaParametros;
import com.mc4.unico.obj.ResultadoDeudas;
import com.mc4.unico.obj.ResultadoPagos;

public interface ColecturiaBeanRemote {
    public ResultadoDeudas getDeudas(String compania, String servicio, ListaIdentificadores claves);
    public ResultadoPagos payDeudas(Double monto, ListaPagoPeriodo periodos, String compania, String servicio, ListaIdentificadores claves, String telefono, String PTM, String extra);
    public ListaParametros getPrmGetDeudas(String compania, String servicio);
    public ListaParametros getPrmPayDeudas(String compania, String servicio);
}
