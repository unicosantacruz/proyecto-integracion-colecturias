package com.mc4.unico.blogic;

import com.mc4.unico.obj.CachePagos;
import com.mc4.unico.obj.Deuda;
import com.mc4.unico.obj.ListaAdelantos;
import com.mc4.unico.obj.ListaDeudas;
import com.mc4.unico.obj.ListaIdentificadores;
import com.mc4.unico.obj.ListaPagoPeriodo;
import com.mc4.unico.obj.ListaParametros;
import com.mc4.unico.obj.ListaTipoCambio;
import com.mc4.unico.obj.Parametro;
import com.mc4.unico.obj.ResultadoDeudas;
import com.mc4.unico.obj.ResultadoPagos;
import com.mc4.unico.obj.TipoCambio;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * *****************************************************************************
 * Componente para consultar deudas y hacer el pago.
 *
 * @author fernando flores
 * @version 1.0.0.02082014
 * *****************************************************************************
 */
@Stateless
public class ColecturiaBean implements ColecturiaBeanRemote {

    private final double CTE_POWER = Math.pow(10, 2);
    private HashMap<Integer, CachePagos> mapCachePagos = new HashMap<Integer, CachePagos>();

    /**
     * *************************************************************************
     * Constructor
     * ************************************************************************
     */
    public ColecturiaBean() {
        //System.out.println("CONTRUCTOR!");
        initializeCache();
    }

    /**
     * *************************************************************************
     * Metodo para consultar las deudas del cliente.
     *
     * @param compania nombre de la compañia, pe. yanbal
     * @param servicio codigo del servicio registrado en unico
     * @param claves secuencias de valores a almacenar y localizar al cliente
     * @return Objeto con el listado de deudas y el monto total
     * ************************************************************************
     */
    @Override
    public ResultadoDeudas getDeudas(String compania, String servicio, ListaIdentificadores claves) {
        ResultadoDeudas result = new ResultadoDeudas();
        Connection cnn = null;
        Statement stm = null, stmex = null;
        ResultSet rs = null, rsex = null;
        String strSelectDeuda, strSelectNombre, strColumna;
        double dblDeudaTotal = 0.0;
        int intColumnCount = 0;
        boolean blnExisteRegistros = false;

        try {
            //------------------------------------------------------------------
            // cargo la configuracion para el servicio
            //------------------------------------------------------------------
            CachePagos cache = getCache(servicio);
            strSelectDeuda = cache.getSelectDeuda();
            strSelectNombre = cache.getSelectNombre();
            //------------------------------------------------------------------
            // creo la conexion a la db
            //------------------------------------------------------------------
            cnn = getConnection();
            stm = cnn.createStatement();
            //------------------------------------------------------------------
            // reemplazo los parametros por nombre
            //------------------------------------------------------------------
            for (Parametro parametro : claves.clave) {
                strColumna = "{" + parametro.nombre + "}";
                strSelectDeuda = strSelectDeuda.replace(strColumna, parametro.valor);
                if (strSelectNombre != null) {
                    strSelectNombre = strSelectNombre.replace(strColumna, parametro.valor);
                }
            }
            //------------------------------------------------------------------
            // recorro el resultado del query de las deudas del cliente
            //------------------------------------------------------------------
            ArrayList<Deuda> listadeudas = new ArrayList<Deuda>();
            ArrayList<Deuda> listaadelantos = new ArrayList<Deuda>();
            rs = stm.executeQuery(strSelectDeuda);
            intColumnCount = rs.getMetaData().getColumnCount();
            while (rs.next()) {
                //System.out.println("----->" + rs.getDouble(1));
                //------------------------------------------------------------------
                // activo un flag que indica q el cliente existe
                //------------------------------------------------------------------
                blnExisteRegistros = true;
                //------------------------------------------------------------------
                // si la deuda es cero -> continuo
                //------------------------------------------------------------------
                if (rs.getDouble(1) == 0d) {
                    continue;
                }
                //------------------------------------------------------------------
                // creo el objeto deuda para la salida y lo lleno
                //------------------------------------------------------------------
                Deuda deuda = new Deuda();
                deuda.monto = Math.abs(rs.getDouble(1));
                if (intColumnCount > 1) {
                    if (rs.getString(2) != null) {
                        deuda.moneda = rs.getString(2);
                    }
                }
                if (intColumnCount > 2) {
                    if (rs.getString(3) != null) {
                        deuda.periodo = rs.getString(3);
                    }
                }
                if (intColumnCount > 3) {
                    if (rs.getString(4) != null) {
                        deuda.detalle = rs.getString(4);
                    }
                }
                if (deuda.moneda == null) {
                    //------------------------------------------------------------------
                    // si no hay informacion de moneda -> se asume bs.
                    //------------------------------------------------------------------
                    dblDeudaTotal += rs.getDouble(1);
                } else if (deuda.moneda.equals("USD")) {
                    //------------------------------------------------------------------
                    // Consulto el tipo de cambio de dolares memorizado
                    //------------------------------------------------------------------
                    double aux = 0d;
                    //------------------------------------------------------------------
                    // Si es la primera vez o es nuevo dia, consulto el tipo de cambio configurado,
                    // Si no encuentra de la fecha, busco de la fecha posterior inmediata
                    // si aun no encuentra busco la ultima fecha configurada
                    //------------------------------------------------------------------
                    for (int i = 0; i < 3; i++) {
                        switch (i) {
                            case 0:
                                strColumna = "select cambio from di_tipo_cambio where fecha=to_number(to_char(sysdate,'YYMMDD'))";
                                break;
                            case 1:
                                strColumna = "select cambio from di_tipo_cambio where fecha=to_number(to_char(sysdate-1,'YYMMDD'))";
                                break;
                            case 2:
                                strColumna = "select cambio from di_tipo_cambio where fecha in (select max(fecha) from di_tipo_cambio)";
                                break;
                            default:
                                strColumna = "";
                        }
                        stmex = cnn.createStatement();
                        rsex = stmex.executeQuery(strColumna);
                        if (rsex.next()) {
                            aux = rsex.getDouble("cambio");
                        }
                        rsex.close();
                        stmex.close();
                        if (aux > 0d) {
                            break;
                        }
                    }
                    //------------------------------------------------------------------
                    // si no existe ningun tio de cambio -> genero un error
                    //------------------------------------------------------------------
                    if (aux == 0d) {
                        throw new Exception("No se encontró ningún tipo de cambio configurado.");
                    }
                    //------------------------------------------------------------------
                    // Si recupero el tipo de cambio, lo aplico en la deuda total y lo memorizo para evitar consultar nuevamente la base de datos
                    //------------------------------------------------------------------
                    ListaTipoCambio objListaTipoCambio = new ListaTipoCambio();
                    objListaTipoCambio.tipo = new TipoCambio[1];
                    objListaTipoCambio.tipo[0] = new TipoCambio(deuda.moneda, aux);
                    result.tipos_cambio = objListaTipoCambio;
                    dblDeudaTotal += rs.getDouble(1) * aux;
                } else {
                    //------------------------------------------------------------------
                    // por defecto se asume deuda en bs.
                    //------------------------------------------------------------------
                    dblDeudaTotal += rs.getDouble(1);
                }
                //------------------------------------------------------------------
                // si el campo deuda es positiva es deuda, si es negativa es adelanto
                //------------------------------------------------------------------
                if (rs.getDouble(1) > 0d) {
                    listadeudas.add(deuda);
                } else {
                    listaadelantos.add(deuda);
                }
            }
            rs.close();
            //------------------------------------------------------------------
            // si existe registros -> el cliente existe
            //------------------------------------------------------------------
            if (blnExisteRegistros) {
                //------------------------------------------------------------------
                // Consulto el nombre del cliente si la consulta esta configurada
                //------------------------------------------------------------------
                if (strSelectNombre != null) {
                    rs = stm.executeQuery(strSelectNombre);
                    if (rs.next()) {
                        result.nombre = rs.getString(1);
                    }
                    rs.close();
                }
                //------------------------------------------------------------------
                // si hay deuda, creo la lista detallada
                //------------------------------------------------------------------
                if (dblDeudaTotal != 0d) {
                    if (listadeudas.size() > 0) {
                        result.deudas = new ListaDeudas();
                        result.deudas.deuda = listadeudas.toArray(new Deuda[listadeudas.size()]);
                        listadeudas.clear();
                    }
                    if (listaadelantos.size() > 0) {
                        result.adelantos = new ListaAdelantos();
                        result.adelantos.adelanto = listaadelantos.toArray(new Deuda[listaadelantos.size()]);
                        listaadelantos.clear();
                    }
                    result.codigo_error = "OK";
                    result.moneda = "BOB";
                    if (dblDeudaTotal >= 0d) {
                        result.total_deuda = Math.round(dblDeudaTotal * CTE_POWER) / CTE_POWER;
                    } else {
                        result.total_adelanto = Math.abs(Math.round(dblDeudaTotal * CTE_POWER) / CTE_POWER);
                    }
                    result.tipo_cobro = "MONTO";  // PERIODO
                    result.tipo_pago = cache.getTipoPagoDescripcion();
                    //------------------------------------------------------------------
                    // este bloque retornaba los limites de los pagos, si desea mandra los limites configurados debe descomentarse este bloque
                    //------------------------------------------------------------------
                    //if (cache.isPagoLimitado() && cache.getTipoPago() != 'T') {
                    //    result.limites = new Limites();
                    //    if (cache.getPagoMaximo() > 0d) {
                    //        if (cache.getPagoMaximo() > dblDeudaTotal) {
                    //            result.limites.pago_maximo = cache.getPagoMaximo();
                    //        } else {
                    //            result.limites.pago_maximo = dblDeudaTotal;
                    //        }
                    //    }
                    //    if (cache.getPagoMinimo() > 0d) {
                    //        result.limites.pago_minimo = cache.getPagoMinimo();
                    //    }
                    //}
                    //------------------------------------------------------------------
                } else {
                    //------------------------------------------------------------------
                    // si el cliente no tiene deuda -> solo existe
                    //------------------------------------------------------------------
                    result.codigo_error = "OK";
                    result.descripcion = "El cliente no tiene deuda.";
                    result.total_deuda = 0.0;
                    result.tipo_cobro = "MONTO";  // PERIODO
                    result.tipo_pago = cache.getTipoPagoDescripcion();
                }
            } else {
                //------------------------------------------------------------------
                // si no encuentra registros de deudas -> se asume que el cliente no existe -> NOK
                //------------------------------------------------------------------
                result.codigo_error = "NOK";
                result.descripcion = "No existe el cliente.";
            }
            //------------------------------------------------------------------
            // cierro conexion db
            //------------------------------------------------------------------
            stm.close();
            cnn.close();
            cnn = null;
        } catch (Exception ex) {
            //------------------------------------------------------------------
            // en cualquier error -> retornar ERROR
            //------------------------------------------------------------------
            if (ex != null) {
                result.codigo_error = "ERROR";
                result.descripcion = ex.getMessage();
            } else {
                result.codigo_error = "ERROR";
                result.descripcion = "Algún parametro de entrada es nulo.";
            }
        } finally {
            //------------------------------------------------------------------
            // cierre de conexion a la db
            //------------------------------------------------------------------
            try {
                if (cnn != null) {
                    if (stmex != null) {
                        if (rsex != null) {
                            if (!rsex.isClosed()) {
                                rsex.close();
                            }
                        }
                        if (!stmex.isClosed()) {
                            stmex.close();
                        }
                    }
                    if (stm != null) {
                        if (rs != null) {
                            if (!rs.isClosed()) {
                                rs.close();
                            }
                        }
                        if (!stm.isClosed()) {
                            stm.close();
                        }
                    }
                    if (!cnn.isClosed()) {
                        cnn.close();
                    }
                }
            } catch (SQLException sqlex) {
                java.util.logging.Logger.getLogger(ColecturiaBean.class.getName()).log(java.util.logging.Level.SEVERE, null, sqlex);
            }
        }
        return result;
    }

    /**
     * *************************************************************************
     * Metodo para registrar un log de error en el pago
     *
     * @param cnn conexion abierta a la base de datos
     * @param monto monto pagado
     * @param servicio codigo del servicio correspondiente a unico
     * @param claves lista de claves
     * @param telefono numero telefono tigo
     * @param PTM numero telefono ptm
     * @param error descripcion del error ocurrido
     * ************************************************************************
     */
    private void putPayFailed(Connection cnn, Double monto, String servicio, ListaIdentificadores claves, String telefono, String PTM, String error) {
        PreparedStatement pstm = null;
        try {
            if (cnn == null) {
                return;
            }
            if (cnn.isClosed()) {
                return;
            }
            //------------------------------------------------------------------
            // valido que los datos para que no genere una excepcion en la db
            //------------------------------------------------------------------
            servicio = (servicio == null ? "" : servicio);
            telefono = (telefono == null ? "" : telefono);
            PTM = (PTM == null ? "" : PTM);
            error = (error == null ? "" : error);
            if (servicio.length() > 20) {
                servicio = servicio.substring(0, 20);
            }
            if (telefono.length() > 50) {
                telefono = telefono.substring(0, 50);
            }
            if (PTM.length() > 50) {
                PTM = PTM.substring(0, 50);
            }
            if (error.length() > 250) {
                error = error.substring(0, 250);
            }
            //------------------------------------------------------------------
            // ejecuto el insert en la tabla de errores
            //------------------------------------------------------------------
            pstm = cnn.prepareStatement("insert into tx_pagos_error (fecha_pago,cod_servicio,monto,nro_cliente,nro_ptm,error,dato1,valor1,dato2,valor2,dato3,valor3,dato4,valor4,dato5,valor5) values (sysdate,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            pstm.setString(1, servicio);
            pstm.setDouble(2, monto);
            pstm.setString(3, telefono);
            pstm.setString(4, PTM);
            pstm.setString(5, error);
            for (int k = 6, c = 0; c < 5; c++, k += 2) {
                if (c < claves.clave.length) {
                    pstm.setString(k, claves.clave[c].nombre);
                    pstm.setString(k + 1, claves.clave[c].valor);
                } else {
                    pstm.setString(k, "");
                    pstm.setString(k + 1, "");
                }
            }
            pstm.execute();
            pstm.close();
            cnn.commit();
        } catch (SQLException ex) {
            //------------------------------------------------------------------
            // en caso de excepcion -> rollback
            //------------------------------------------------------------------
            try {
                cnn.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(ColecturiaBean.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } finally {
            //------------------------------------------------------------------
            // cierro el statement
            //------------------------------------------------------------------
            if (pstm != null) {
                try {
                    if (!pstm.isClosed()) {
                        pstm.close();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(ColecturiaBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * *************************************************************************
     * Metedo para el pago de deudas
     *
     * @param monto monto del pago
     * @param periodos lista de periodos (para uso futuro cuando se haga el pago
     * por periodos)
     * @param compania nombre de la empresa
     * @param servicio codigo de servicio registrado en unico
     * @param claves lista de claves para localizar al cliente
     * @param telefono numero telefono tigo
     * @param PTM numero telefono ptm
     * @param extra parametro extra (para caso de pasos adicional, sin uso
     * actuamente)
     * @return retorna "OK", "NOK" cuando no existe el cliente, "ERROR" por una
     * falla interna
     * *************************************************************************
     */
    @Override
    public ResultadoPagos payDeudas(Double monto, ListaPagoPeriodo periodos, String compania, String servicio, ListaIdentificadores claves, String telefono, String PTM, String extra) {
        ResultadoPagos result = new ResultadoPagos();
        Connection cnn = null;
        PreparedStatement pstm = null;
        CallableStatement cstm = null;
        String query;
        String[] vecDatosExtra = new String[6];
        String[] vecClaveCliente = new String[3];
        int idxClaveCliente = 0;
        int idxDatoExtra = 0;

        try {
            //------------------------------------------------------------------
            // validar al menos un ID para identificar el cliente
            //------------------------------------------------------------------
            if (claves.clave.length == 0) {
                throw new Exception("No hay argumentos para identificar la cuenta.");
            }
            //------------------------------------------------------------------
            // recupero la configuracion del servicio y reemplazo la expresion "{$}" del pago
            //------------------------------------------------------------------
            CachePagos cache = getCache(servicio);
            query = cache.getFuncionPago();
            query = query.replace("{$}", monto.toString());
            //------------------------------------------------------------------
            // reemplazo los parametros por la sintaxis de posicion de columnas {N}
            //------------------------------------------------------------------
            String straux;
            for (Parametro parametro : claves.clave) {
                straux = "{" + parametro.nombre + "}";
                if (query.contains(straux)) {
                    query = query.replace(straux, parametro.valor);
                    if (idxClaveCliente < 3) {
                        vecClaveCliente[idxClaveCliente] = parametro.valor;
                        idxClaveCliente++;
                    }
                } else {
                    idxDatoExtra = cache.getDatoExtra(parametro.nombre);
                    vecDatosExtra[idxDatoExtra] = parametro.valor;
                }
            }
            //------------------------------------------------------------------
            // ejecuto la llamada a la funcion que maneja la logica del pago
            //------------------------------------------------------------------
            query = "{?=call " + query + "}";
            cnn = getConnection();
            cstm = cnn.prepareCall(query);
            cstm.registerOutParameter(1, Types.VARCHAR);
            cstm.executeQuery();
            String strExeResponse = cstm.getString(1);
            cstm.close();
            //------------------------------------------------------------------
            // el resultado de la funcion debe tener el formato: "OK;<deuda inicial>;<pago:cuenta:tipo_cambio>"
            // si el resultado es diferente, se toma como error
            //------------------------------------------------------------------
            String[] vecSqlResponse = strExeResponse.split(";");
            if (!vecSqlResponse[0].equals("OK")) {
                if (vecSqlResponse.length > 1) {
                    throw new Exception(vecSqlResponse[1]);
                } else {
                    throw new Exception(vecSqlResponse[0]);
                }
            }
            //------------------------------------------------------------------
            // registro el pago
            //------------------------------------------------------------------
            String[] vecDetallePago;
            Double subDataMonto = 0.0;
            String subDataCuenta = "";
            Double subTipoCambio = 0.0;
            Timestamp timestamp = getCurrentTimeStamp();
            query = "insert into tx_pagos (estado,id_pago,monto,fecha_pago,nro_cliente,nro_ptm,nro_extra,cuenta,clave1,clave2,clave3,dato1,dato2,dato3,dato4,dato5,deuda_inicial,tipo_cambio) values ('NEW',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            pstm = cnn.prepareStatement(query);
            pstm.setInt(1, cache.getIdPago());
            pstm.setTimestamp(3, timestamp);
            pstm.setString(4, telefono);
            pstm.setString(5, PTM);
            pstm.setString(6, extra);
            pstm.setString(8, vecClaveCliente[0]);
            pstm.setString(9, vecClaveCliente[1]);
            pstm.setString(10, vecClaveCliente[2]);
            pstm.setString(11, vecDatosExtra[1]);
            pstm.setString(12, vecDatosExtra[2]);
            pstm.setString(13, vecDatosExtra[3]);
            pstm.setString(14, vecDatosExtra[4]);
            pstm.setString(15, vecDatosExtra[5]);
            pstm.setDouble(16, Double.parseDouble(vecSqlResponse[1]));
            for (int i = 2; i < vecSqlResponse.length; i++) {
                if (vecSqlResponse[i].contains(":")) {
                    vecDetallePago = vecSqlResponse[i].split(":");
                    subDataMonto = Double.parseDouble(vecDetallePago[0]);
                    subDataCuenta = vecDetallePago[1];
                    subTipoCambio = Double.parseDouble(vecDetallePago[2]);
                } else {
                    subDataMonto = Double.parseDouble(vecSqlResponse[i]);
                    subDataCuenta = "";
                    subTipoCambio = 0d;
                }
                pstm.setDouble(2, subDataMonto);
                pstm.setString(7, subDataCuenta);
                pstm.setDouble(17, subTipoCambio);
                pstm.execute();
            }
            pstm.close();
            cnn.commit();
            cnn.close();
            cnn = null;
            //------------------------------------------------------------------
            // si todo termino sin probelmas -> OK
            //------------------------------------------------------------------
            result.codigo_error = "OK";
        } catch (SQLException sqlex) {
            //------------------------------------------------------------------
            // alguna excepcion db -> rollback
            //------------------------------------------------------------------
            try {
                if (cnn != null) {
                    cnn.rollback();
                }
            } catch (SQLException ex) {
                Logger.getLogger(ColecturiaBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            result.codigo_error = "ERROR";
            result.descripcion = sqlex.getMessage();
            putPayFailed(cnn, monto, servicio, claves, telefono, PTM, sqlex.getMessage());
        } catch (Exception ex) {
            result.codigo_error = "ERROR";
            result.descripcion = ex.getMessage();
            putPayFailed(cnn, monto, servicio, claves, telefono, PTM, ex.getMessage());
        } finally {
            //------------------------------------------------------------------
            // libero elemntos db
            //------------------------------------------------------------------
            try {
                if (cnn != null) {
                    if (cstm != null) {
                        if (!cstm.isClosed()) {
                            cstm.close();
                        }
                    }
                    if (pstm != null) {
                        if (!pstm.isClosed()) {
                            pstm.close();
                        }
                    }
                    if (!cnn.isClosed()) {
                        cnn.close();
                    }
                }
            } catch (SQLException sqlex) {
                Logger.getLogger(ColecturiaBean.class.getName()).log(Level.SEVERE, null, sqlex);
            }
        }
        return result;
    }

    /**
     * *************************************************************************
     * Metodo que retorna la configuracion de pagos memorizada correspondiente a
     * un codigo de servicio
     *
     * @param servicio codigo del servicio registrado en unico
     * @return Una estructura con la configuracion
     * @throws Exception
     * *************************************************************************
     */
    private CachePagos getCache(String servicio) throws Exception {
        try {
            CachePagos result = mapCachePagos.get(Integer.parseInt(servicio));
            if (result == null) {
                throw new Exception("El codigo de servicio '" + servicio + "' no esta configurado u ocurríó un error al recuperar su configuración.");
            } else {
                return result;
            }
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    /**
     * *************************************************************************
     * Funcion para obtener el listado de parametros que el pago de una empresa
     * requiere
     *
     * @param strExpresion Expresion que contiene la entrada de parametros bajo
     * el formato "xxx{algo1}xx{algo2}xxx"
     * @param vecExtras lista de parametros extra
     * @return
     * ************************************************************************
     */
    private ListaParametros getParametros(String strExpresion, String[] vecExtras) {
        ListaParametros result = null;
        String strin = "";
        ArrayList<String> vecTemporal = new ArrayList<String>();

        if (strExpresion != null) {
            //------------------------------------------------------------------
            // inicio un loop que encuentra parametros de entrada con sintaxis "xxx{parametros}xx"
            //------------------------------------------------------------------
            int p = strExpresion.indexOf("{"), q = 0;
            while (p > 0) {
                q = strExpresion.indexOf("}", p);
                if (q > 0) {
                    strin = strExpresion.substring(p + 1, q);
                    if (strin.length() > 1) {
                        vecTemporal.add(strin);
                    }
                    p = strExpresion.indexOf("{", q);
                } else {
                    break;
                }
            }
            //------------------------------------------------------------------
            // añadir parametros adicionales propios para tigomoney, pe. ci
            //------------------------------------------------------------------
            if (vecExtras != null) {
                for (String valor : vecExtras) {
                    vecTemporal.add(valor);
                }
            }
            //------------------------------------------------------------------
            // convertir a array
            //------------------------------------------------------------------
            if (vecTemporal.size() > 0) {
                result = new ListaParametros();
                result.parametro = vecTemporal.toArray(new String[vecTemporal.size()]);
            }
        }
        return result;
    }

    /**
     * *************************************************************************
     * Metodo que obtiene la lista de parametros la consulta de deudas necesita
     *
     * @param compania nombre de la empresa (sin uso en este nivel)
     * @param servicio codigo del servicio registrado en unico
     * @return Lista de parametrso para la consulta de deudas
     * ************************************************************************
     */
    @Override
    public ListaParametros getPrmGetDeudas(String compania, String servicio) {
        try {
            CachePagos cache = getCache(servicio);
            return getParametros(cache.getSelectDeuda(), null);
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * *************************************************************************
     * Metodo que obtiene la lista de parametrso necesarioa para el pago de
     * deudas
     *
     * @param compania nombre de la empresa (sin uso en este nivel)
     * @param servicio codigo del servcio registrado en unico
     * @return Lista de parametros para el pago
     * ************************************************************************
     */
    @Override
    public ListaParametros getPrmPayDeudas(String compania, String servicio) {
        try {
            CachePagos cache = getCache(servicio);
            return getParametros(cache.getFuncionPago(), cache.getDatosExtra());
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * *************************************************************************
     * Crea la conexion a la base de datos, es necesario tener un data source en
     * el SA
     *
     * @return la conexion abierta
     * @throws NamingException
     * @throws SQLException
     * ************************************************************************
     */
    private Connection getConnection() throws NamingException, SQLException {
        Context ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup("jdbc/unico_collector_ds");
        Connection con = ds.getConnection();
        con.setAutoCommit(false);
        return con;
    }

    /**
     * *************************************************************************
     * Formate la hora y fecha actual a Timestap para registro en la base de
     * datos
     *
     * @return
     * ************************************************************************
     */
    private static java.sql.Timestamp getCurrentTimeStamp() {
        java.util.Date today = new java.util.Date();
        return new java.sql.Timestamp(today.getTime());
    }

    /**
     * *************************************************************************
     * Llena la estrutura de configuraciones de pago, emorizandolo
     * ************************************************************************
     */
    private void initializeCache() {
        Connection cnn = null;
        Statement stm = null;
        ResultSet rs = null;
        String query;

        try {
            //------------------------------------------------------------------
            // abro conexion db
            //------------------------------------------------------------------
            mapCachePagos.clear();
            cnn = getConnection();
            stm = cnn.createStatement();
            //------------------------------------------------------------------
            // cargo la configuracion para pagos
            //------------------------------------------------------------------
            query = "select to_number(cod_servicio) P1,tipo P2,dmlselect P3,funpago_total P4,funpago_parcial P5,funpago_adelanto P6,funpago_periodo P7,id_pago P8,dmlnombre P9,pago_minimo P10,pago_maximo P11,dato1 P12,dato2 P13,dato3 P14,dato4 P15,dato5 P16 from di_pagos where estado='ON'";
            rs = stm.executeQuery(query);
            while (rs.next()) {
                mapCachePagos.put(rs.getInt("P1"), new CachePagos(rs.getInt("P8"), rs.getString("P3"), rs.getString("P4"), rs.getString("P5"), rs.getString("P6"), rs.getString("P7"), rs.getString("P9"), rs.getInt("P10"), rs.getInt("P11"), rs.getString("P2").charAt(0), rs.getString("P12"), rs.getString("P13"), rs.getString("P14"), rs.getString("P15"), rs.getString("P16")));
            }
            rs.close();
            stm.close();
            cnn.close();
            cnn = null;
        } catch (SQLException ex) {
            System.out.println("EXCEPCION:" + ex.getMessage());
            mapCachePagos.clear();
        } catch (NamingException ex) {
            System.out.println("EXCEPCION:" + ex.getMessage());
            mapCachePagos.clear();
        } finally {
            try {
                if (cnn != null) {
                    if (stm != null) {
                        if (rs != null) {
                            if (!rs.isClosed()) {
                                rs.close();
                            }
                        }
                        if (!stm.isClosed()) {
                            stm.close();
                        }
                    }
                    if (!cnn.isClosed()) {
                        cnn.close();
                    }
                }
            } catch (SQLException sqlex) {
                java.util.logging.Logger.getLogger(ColecturiaBean.class.getName()).log(java.util.logging.Level.SEVERE, null, sqlex);
            }
        }
    }

}
