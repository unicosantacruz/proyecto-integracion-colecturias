<%-- 
    Document   : index
    Created on : 1/08/2014, 06:54:39 PM
    Author     : fernando flores
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" import="com.mc4.unico.com.AccesoColecturia"%>
<%  
    String operacion = request.getParameter("operacion");
    if (operacion != null) {
        if (operacion.equals("import") || operacion.equals("export")) {
            String empresa = request.getParameter("empresa");
            String servicio = request.getParameter("servicio");
            String modo = request.getParameter("modo");
            if (empresa != null && servicio != null && modo != null) {
                if (modo.equals("normal")) {
                    response.getWriter().print("<h3>");
                    AccesoColecturia acceso = new AccesoColecturia();
                    response.getWriter().print(acceso.getFormattedDate());
                    if (operacion.equals("import")) {
                        response.getWriter().print(" Importacion: ");
                        response.getWriter().print(acceso.executar_proceso_import(empresa, servicio));
                    } else {
                        response.getWriter().print(" Exportacion: ");
                        response.getWriter().print(acceso.executar_proceso_export(empresa, servicio));
                    }
                    response.getWriter().print("</h3>");
                } else if (modo.equals("background")) {
                    response.getWriter().print("<h3>");
                    AccesoColecturia acceso = new AccesoColecturia(empresa, servicio);
                    response.getWriter().print(acceso.getFormattedDate());
                    response.getWriter().print(" ");
                    if (operacion.equals("import")) {
                        acceso.executar_servicio_import();
                        response.getWriter().print("Importacion iniciada.");
                    } else {
                        acceso.executar_servicio_export();
                        response.getWriter().print("Exportacion iniciada.");
                    }
                    response.getWriter().print("</h3>");
                }
            }
        } else if (operacion.equals("encrypt")) {
            String expresion = request.getParameter("expresion");
            if (expresion != null) {
                response.getWriter().print("<h3>");
                AccesoColecturia acceso = new AccesoColecturia();
                response.getWriter().print(acceso.getFormattedDate());
                response.getWriter().print(" ");
                response.getWriter().print(acceso.encriptar(expresion));
                response.getWriter().print("</h3>");
            }
        }
    } else {
        response.getWriter().print("<h3>SERVICIO EN OPERACION</h3>");
    }
%>