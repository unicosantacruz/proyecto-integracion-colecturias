package com.mc4.unico.com;

import com.mc4.unico.blogic.ImportBeanRemote;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

    /***************************************************************************
 * Clase que enmascara los metodos de importacion y exportacion de archivo de
 * datos
 * @author fernando flores
 * @version 1.0.0.02082014
 ******************************************************************************/
public class AccesoColecturia {

    private final String CTE_COLECTOR_URL = "com.mc4.unico.blogic.ImportBeanRemote";
    private final String subEmpresa;
    private final String subServicio;

    /***************************************************************************
     * Contructor
     * @param subEmpresa nombre de empresa, pe. yanbal
     * @param subServicio nombre del servicio, pe. colecturia, pospago
     **************************************************************************/
    public AccesoColecturia(String subEmpresa, String subServicio) {
        this.subEmpresa = subEmpresa;
        this.subServicio = subServicio;
    }

    /***************************************************************************
     * Constructor
     **************************************************************************/
    public AccesoColecturia() {
        this.subEmpresa = null;
        this.subServicio = null;
    }

    /***************************************************************************
     * Encripta una expresion
     * @param expresion palabra a encriptar
     * @return expresion encriptada
     **************************************************************************/
    public String encriptar(String expresion) {
        try {
            Context c = new InitialContext();
            ImportBeanRemote ejbref = (ImportBeanRemote) c.lookup(CTE_COLECTOR_URL);  //"java:global/UnicoImport/ImportBean"  //"java:global/UnicoImport/ImportBean!com.mc4.unico.blogic.ImportBeanRemote"
            return ejbref.encrypt(expresion);
        } catch (NamingException ex) {
            Logger.getLogger(AccesoColecturia.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }

    }

    /***************************************************************************
     * Ejecuta la importacion
     * @param empresa nombre de empresa
     * @param servicio nombre de servicio
     * @return 
     **************************************************************************/
    public String executar_proceso_import(String empresa, String servicio) {
        try {
            Context c = new InitialContext();
            ImportBeanRemote ejbref = (ImportBeanRemote) c.lookup(CTE_COLECTOR_URL);
            return ejbref.collect(empresa, servicio);
        } catch (NamingException ex) {
            return ex.getMessage();
        }
    }

    /***************************************************************************
     * Ejecuta la exportacion
     * @param empresa nombre de empresa
     * @param servicio nombre de servicio
     * @return 
     **************************************************************************/
    public String executar_proceso_export(String empresa, String servicio) {
        try {
            Context c = new InitialContext();
            ImportBeanRemote ejbref = (ImportBeanRemote) c.lookup(CTE_COLECTOR_URL);
            return ejbref.export(empresa, servicio);
        } catch (NamingException ex) {
            return ex.getMessage();
        }
    }

    /***************************************************************************
     * Inicia la exportacion en background
     **************************************************************************/
    public void executar_servicio_export() {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                try {
                    Context c = new InitialContext();
                    ImportBeanRemote ejbref = (ImportBeanRemote) c.lookup(CTE_COLECTOR_URL);
                    ejbref.export(subEmpresa, subServicio);
                } catch (NamingException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        };
        new Thread(r).start();
    }

    /***************************************************************************
     * Inicia la importacion en background
     **************************************************************************/
    public void executar_servicio_import() {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                try {
                    Context c = new InitialContext();
                    ImportBeanRemote ejbref = (ImportBeanRemote) c.lookup(CTE_COLECTOR_URL);
                    ejbref.collect(subEmpresa, subServicio);
                } catch (NamingException ex) {
                }
            }
        };
        new Thread(r).start();
    }
    
    public String getFormattedDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(new Date());
    }
}
