/*******************************************************************************
HISTORICO
21-07-2014  Fernando Flores Creacion.
01-08-2014  Fernando Flores Actualizacion.
04-08-2014  Fernando Flores Modifico para que el nombre de archivo de salida tenga la hora y corregir pagos adelantados en dolares
05-08-2014  Fernando Flores Coloco / para compatibilidad con sqldevelopment
*******************************************************************************/

/*******************************************************************************
di_tipo_cambio
*******************************************************************************/

CREATE TABLE unico.di_tipo_cambio
    (moneda                         CHAR(3) NOT NULL,
    cambio                         NUMBER(9,2) NOT NULL,
    fecha                          NUMBER(6,0) NOT NULL);
    
ALTER TABLE unico.di_tipo_cambio
ADD CONSTRAINT pk_cambio_fecha PRIMARY KEY (fecha)
USING INDEX;

ALTER TABLE unico.di_tipo_cambio
ADD CONSTRAINT ck_cambio_moneda CHECK (moneda in ('BOB','USD','EUR'));

COMMENT ON COLUMN unico.di_tipo_cambio.cambio IS 'Tipo de cambio a bolivianos.';
COMMENT ON COLUMN unico.di_tipo_cambio.fecha IS 'Fecha en formato YYMMDD';
COMMENT ON COLUMN unico.di_tipo_cambio.moneda IS 'Tipo de moneda, pe.USD,EUR';
/
    
/*******************************************************************************
di_email
*******************************************************************************/
CREATE TABLE unico.di_email
    (id_email                       NUMBER(3,0) NOT NULL,
    direccion                      VARCHAR2(50) NOT NULL);

ALTER TABLE unico.di_email
ADD CONSTRAINT pk_email_direccion UNIQUE (direccion)
USING INDEX;

ALTER TABLE unico.di_email
ADD CONSTRAINT pk_email PRIMARY KEY (id_email)
USING INDEX;

COMMENT ON COLUMN unico.di_email.direccion IS 'Direccion de correo electronico';
COMMENT ON COLUMN unico.di_email.id_email IS 'ID del email configurado.';

CREATE SEQUENCE unico.sec_id_email
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 999
  NOCYCLE
  NOORDER
  CACHE 20;
/

/*******************************************************************************
di_email_servicio
*******************************************************************************/

CREATE TABLE unico.di_email_servicio
    (id_email                       NUMBER(3,0) NOT NULL,
    cod_servicio                   VARCHAR2(20) NOT NULL,
    alerta_errores                 NUMBER(1,0) DEFAULT 0 NOT NULL,
    alerta_eventos                 NUMBER(1,0) DEFAULT 0 NOT NULL,
    estado                         CHAR(2) DEFAULT 'ON' NOT NULL);

ALTER TABLE unico.di_email_servicio
ADD CONSTRAINT ck_email_estado CHECK (estado in ('ON','OF'));

COMMENT ON COLUMN unico.di_email_servicio.alerta_errores IS 'Indica si el envio aplica por errores.';
COMMENT ON COLUMN unico.di_email_servicio.alerta_eventos IS 'Indica si el envio aplica para notificar eventos.';
COMMENT ON COLUMN unico.di_email_servicio.cod_servicio IS 'ID relacion con la tabla de servicios BC_SERVICIO.';
COMMENT ON COLUMN unico.di_email_servicio.estado IS 'Indica si el envio esta activado: ON, OF';
COMMENT ON COLUMN unico.di_email_servicio.id_email IS 'ID relacion con la tabla de configuracion de correos.';

ALTER TABLE unico.di_email_servicio
ADD CONSTRAINT fk_email_servicios_1 FOREIGN KEY (id_email)
REFERENCES unico.di_email (id_email);
/

/*******************************************************************************
di_conexion
*******************************************************************************/

CREATE TABLE unico.di_conexion
    (id_conexion                    NUMBER(9,0) NOT NULL,
    nombre                         VARCHAR2(50) NOT NULL,
    protocolo                      VARCHAR2(25) NOT NULL);

ALTER TABLE unico.di_conexion
ADD CONSTRAINT ck_protocolo CHECK (protocolo in ('FTP','SOAP','SFTP','SCP','POST','GET'));

ALTER TABLE unico.di_conexion
ADD CONSTRAINT pk_conexion PRIMARY KEY (id_conexion)
USING INDEX;

COMMENT ON COLUMN unico.di_conexion.id_conexion IS 'ID de la conexion configurada.';
COMMENT ON COLUMN unico.di_conexion.nombre IS 'Nombre de la conexion, pe. conexion FTP al repositorio de colecturias.';
COMMENT ON COLUMN unico.di_conexion.protocolo IS 'Protocolo de comunicacion, siglas que itentifican el medio, pe. FTP, SFTP';

CREATE SEQUENCE unico.sec_id_conexion
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 999999999
  NOCYCLE
  NOORDER
  CACHE 20;
/

/*******************************************************************************
di_atributo_conexion
*******************************************************************************/
CREATE TABLE unico.di_atributo_conexion
    (id_conexion                    NUMBER(9,0) NOT NULL,
    nombre                         VARCHAR2(50) NOT NULL,
    valor                          VARCHAR2(200) NOT NULL,
    encriptado                     NUMBER(1,0) DEFAULT 0 NOT NULL);

CREATE UNIQUE INDEX unico.dx_conexion ON unico.di_atributo_conexion
  (
    id_conexion                     ASC,
    nombre                          ASC
  );

ALTER TABLE unico.di_atributo_conexion
ADD CONSTRAINT ck_atributo_encriptado CHECK (ENCRIPTADO=0 OR ENCRIPTADO=1);

COMMENT ON COLUMN unico.di_atributo_conexion.encriptado IS 'Indica si el valor esta encriptado (valor>0), si esta encriptado la encriptacion debe acerce por el aplicativo standalone.';
COMMENT ON COLUMN unico.di_atributo_conexion.id_conexion IS 'ID de relacion con la tabla de configuracion de conexiones.';
COMMENT ON COLUMN unico.di_atributo_conexion.nombre IS 'Nombre del atributo, pe. ip,host,usuario,etc.';
COMMENT ON COLUMN unico.di_atributo_conexion.valor IS 'Valor del atributo';

ALTER TABLE unico.di_atributo_conexion
ADD CONSTRAINT fk_conexion FOREIGN KEY (id_conexion)
REFERENCES UNICO.di_conexion (id_conexion) ON DELETE CASCADE;
/

/*******************************************************************************
di_exportacion
*******************************************************************************/
    
CREATE TABLE unico.di_exportacion
    (id_exportacion                 NUMBER(9,0) NOT NULL,
    cod_servicio                   VARCHAR2(20) NOT NULL,
    id_conexion                    NUMBER(9,0) NOT NULL,
    estado                         CHAR(2) NOT NULL,
    extension_salida               VARCHAR2(10),
    directorio_salida              VARCHAR2(250) NOT NULL,
    separador                      VARCHAR2(5) NOT NULL,
    directorio_temporal            VARCHAR2(250),
    tipo_salida                    CHAR(3) NOT NULL,
    query_datos                    VARCHAR2(500),
    query_nombre                   VARCHAR2(500),
    filtro_cuenta                  VARCHAR2(15),
    extension_proceso              VARCHAR2(10) NOT NULL,
    descripcion                    VARCHAR2(50));

ALTER TABLE unico.di_exportacion
ADD CONSTRAINT ck_exportacion_estado CHECK (estado IN ('ON','OF'));

ALTER TABLE unico.di_exportacion
ADD CONSTRAINT pk_exportacion PRIMARY KEY (id_exportacion)
USING INDEX;

COMMENT ON COLUMN unico.di_exportacion.cod_servicio IS 'ID de relacion con la tabla de BC_SERVICIOS';
COMMENT ON COLUMN unico.di_exportacion.descripcion IS 'Descripcion de la exportacion, pe. Exportacion de pagos yanbal';
COMMENT ON COLUMN unico.di_exportacion.directorio_salida IS 'Directorio de archivos de salida';
COMMENT ON COLUMN unico.di_exportacion.directorio_temporal IS 'Directorio LOCAL donde se crearan archivos temporales.';
COMMENT ON COLUMN unico.di_exportacion.estado IS 'Estado de la configuracion de exportacion: ON,OF';
COMMENT ON COLUMN unico.di_exportacion.extension_proceso IS 'Extension de los archivos en proceso de escritura, pe. .pro, .tmp';
COMMENT ON COLUMN unico.di_exportacion.extension_salida IS 'Extension del nombre del archivo de salida, pe. .out, .salida';
COMMENT ON COLUMN unico.di_exportacion.filtro_cuenta IS 'Filtro de los pagos por el campo TX_PAGOS.CUENTA (esto se hizo para soportar multiple moneda de yanbal).';
COMMENT ON COLUMN unico.di_exportacion.id_conexion IS 'ID de relacion con la tabla de conexiones';
COMMENT ON COLUMN unico.di_exportacion.id_exportacion IS 'ID de la la configuracion de exportacion, este debe generase con la secuencia respectiva.';
COMMENT ON COLUMN unico.di_exportacion.query_datos IS 'Query de la tabla TX_PAGOS de datos que debe corresponder a las columnas que tendra el archivo de salida.';
COMMENT ON COLUMN unico.di_exportacion.query_nombre IS 'Query que genera el nombre del archivo de salida.';
COMMENT ON COLUMN unico.di_exportacion.separador IS 'Separador entre columnas, pe. /t';
COMMENT ON COLUMN unico.di_exportacion.tipo_salida IS 'Tipo de salida: TXT=texto plano, ZIP=el archivo de salida ser� comprimido (no probado completamente)';

ALTER TABLE unico.di_exportacion
ADD CONSTRAINT fk_conexion_exportacion FOREIGN KEY (id_conexion)
REFERENCES unico.di_conexion (id_conexion) ON DELETE CASCADE;

CREATE SEQUENCE unico.sec_id_exportacion
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 999999999
  NOCYCLE
  NOORDER
  CACHE 20;
/

/*******************************************************************************
di_importacion
*******************************************************************************/
    
CREATE TABLE unico.di_importacion
    (id_importacion                 NUMBER(9,0) NOT NULL,
    cod_servicio                   VARCHAR2(20) NOT NULL,
    id_conexion                    NUMBER(9,0) NOT NULL,
    estado                         CHAR(2) NOT NULL,
    separador                      VARCHAR2(5) NOT NULL,
    extension_entrada              VARCHAR2(10) NOT NULL,
    directorio_entrada             VARCHAR2(250) NOT NULL,
    extension_salida               VARCHAR2(10) NOT NULL,
    directorio_salida              VARCHAR2(250),
    linea_inicial                  NUMBER(1,0) DEFAULT 1 NOT NULL,
    dmldelete                      VARCHAR2(500),
    dmlinsert                      VARCHAR2(500) NOT NULL,
    extension_rechazo              VARCHAR2(10) NOT NULL,
    directorio_rechazo             VARCHAR2(250),
    dmlupdate                      VARCHAR2(500),
    prefijo_entrada                VARCHAR2(10),
    ordenacion                     NUMBER(1,0) DEFAULT 0 NOT NULL,
    directorio_temporal            VARCHAR2(250),
    tipo_entrada                   CHAR(3) DEFAULT 'TXT' NOT NULL,
    extension_unzip                VARCHAR2(10),
    prefijo_unzip                  VARCHAR2(10),
    dmlinicial                     VARCHAR2(500),
    bloque_transaccion             NUMBER(4,0) DEFAULT 1000 NOT NULL,
    limite_errores                 NUMBER(2,0) DEFAULT 0 NOT NULL,
    extension_proceso              VARCHAR2(10) NOT NULL,
    descripcion                    VARCHAR2(50) NOT NULL,
    dmlerror                       VARCHAR2(500));
    
ALTER TABLE unico.di_importacion
ADD CONSTRAINT ck_tipo_entrada CHECK (TIPO_ENTRADA IN ('TXT','ZIP'));

ALTER TABLE unico.di_importacion
ADD CONSTRAINT ck_importacion_estado CHECK (ESTADO IN ('ON','OF'));

ALTER TABLE unico.di_importacion
ADD CONSTRAINT pk_importacion PRIMARY KEY (id_importacion)
USING INDEX;

ALTER TABLE unico.di_importacion
ADD CONSTRAINT fk_conexion_importacion FOREIGN KEY (id_conexion)
REFERENCES UNICO.di_conexion (id_conexion) ON DELETE CASCADE;

COMMENT ON COLUMN unico.di_importacion.bloque_transaccion IS 'Indica el numero de registros a consolidar en base de datos (COMMIT).';
COMMENT ON COLUMN unico.di_importacion.cod_servicio IS 'ID de relacion con la tabla BC_SERVICIOS';
COMMENT ON COLUMN unico.di_importacion.descripcion IS 'Descripcion de la configuracion de importacion.';
COMMENT ON COLUMN unico.di_importacion.directorio_entrada IS 'Directorio donde estan los archivos de entrada';
COMMENT ON COLUMN unico.di_importacion.directorio_rechazo IS 'Directorio de salida de archivos rechazados, entendiendose archivo rechazados por archivos que no se cargaron completamente.';
COMMENT ON COLUMN unico.di_importacion.directorio_salida IS 'Directorio de salida de los archivos procesados correctamente.';
COMMENT ON COLUMN unico.di_importacion.directorio_temporal IS 'Directorio temporal LOCAL donde se traeran los archivos para procesarse.';
COMMENT ON COLUMN unico.di_importacion.dmldelete IS 'DELETE que se ejecutara por cada linea leida del archivo.';
COMMENT ON COLUMN unico.di_importacion.dmlinicial IS 'Algun DML que se ejecutar� por cada archivo de entrada.';
COMMENT ON COLUMN unico.di_importacion.dmlinsert IS 'INSERT que se ejecutara por cada linea leida del archivos.';
COMMENT ON COLUMN unico.di_importacion.dmlupdate IS 'UPDATE que se ejecutara por cada linea leida del archivo.';
COMMENT ON COLUMN unico.di_importacion.estado IS 'Estado del registro: ON, OF';
COMMENT ON COLUMN unico.di_importacion.extension_entrada IS 'Extension de los archivos de entrada, pe. .in, .entrada';
COMMENT ON COLUMN unico.di_importacion.extension_proceso IS 'Extension para marcar los archivos en proceso, pe. .tmp, .pro';
COMMENT ON COLUMN unico.di_importacion.extension_rechazo IS 'Extension de archivos rechazados por al menos una falla en el archivo.';
COMMENT ON COLUMN unico.di_importacion.extension_salida IS 'Extension del archivo salida, pe. .salida, .out';
COMMENT ON COLUMN unico.di_importacion.extension_unzip IS 'Extension de los archivos contenidos en el paquete ZIP (sin probarse completamente)';
COMMENT ON COLUMN unico.di_importacion.id_conexion IS 'ID de relacion con la tabla de conexiones';
COMMENT ON COLUMN unico.di_importacion.id_importacion IS 'ID del la configuracion de importacion.';
COMMENT ON COLUMN unico.di_importacion.limite_errores IS 'Maximo numero de lineas en error antes de dejar el proceso. (sin probar completamente)';
COMMENT ON COLUMN unico.di_importacion.linea_inicial IS 'Numero de linea inicial de datos en los archivos de entrada.';
COMMENT ON COLUMN unico.di_importacion.ordenacion IS 'Tipo de ordenacion de archivos: 0=ninguna, 1=acendete del principio, 2=acendente desde el final';
COMMENT ON COLUMN unico.di_importacion.prefijo_entrada IS 'Prefijo de los archivos de entrada, pe. YANB, YAND';
COMMENT ON COLUMN unico.di_importacion.prefijo_unzip IS 'Prefijo de los archivos contenidos en el paquete ZIP (sin probarse completamente)';
COMMENT ON COLUMN unico.di_importacion.separador IS 'Separador de columnas del archivo a importar, pe. /t';
COMMENT ON COLUMN unico.di_importacion.tipo_entrada IS 'Indica el tipo de archivo de entrada: TXT=texto plano, ZIP comprimido (sin probarse completamente)';
COMMENT ON COLUMN unico.di_importacion.dmlerror IS 'DML que se ejecutar� ante cualquier error en la importacion.';

CREATE SEQUENCE unico.sec_id_importacion
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 999999999
  NOCYCLE
  NOORDER
  CACHE 20;
/

/*******************************************************************************
di_pagos
*******************************************************************************/
    
CREATE TABLE unico.di_pagos
    (id_pago                        NUMBER(9,0) NOT NULL,
    cod_servicio                   VARCHAR2(20) NOT NULL,
    dmlselect                      VARCHAR2(500) NOT NULL,
    tipo                           CHAR(3) NOT NULL,
    estado                         CHAR(2) NOT NULL,
    moneda                         CHAR(3) NOT NULL,
    funpago_total                  VARCHAR2(500),
    funpago_parcial                VARCHAR2(500),
    funpago_adelanto               VARCHAR2(500),
    funpago_periodo                VARCHAR2(500),
    dmlnombre                      VARCHAR2(500),
    pago_minimo                    NUMBER(7,0) NOT NULL,
    pago_maximo                    NUMBER(7,0) NOT NULL,
    dato1                          VARCHAR2(10),
    dato2                          VARCHAR2(10),
    dato3                          VARCHAR2(10),
    dato4                          VARCHAR2(10),
    dato5                          VARCHAR2(10));


ALTER TABLE unico.di_pagos
ADD CONSTRAINT ck_pagos_moneda CHECK (MONEDA IN ('BOB','USD','EUR'));

ALTER TABLE unico.di_pagos
ADD CONSTRAINT ck_pagos_estado CHECK (estado in ('ON','OF'));

ALTER TABLE unico.di_pagos
ADD CONSTRAINT pk_pago PRIMARY KEY (id_pago)
USING INDEX;

ALTER TABLE unico.di_pagos
ADD CONSTRAINT ck_tipo_pago CHECK (TIPO IN ('TOT','PAR','ADE'));


COMMENT ON COLUMN unico.di_pagos.cod_servicio IS 'ID de relacion con la tabla BC_SERVICIO.';
COMMENT ON COLUMN unico.di_pagos.dato1 IS 'Nombre de dato adicional que no requiere la empresa pero Tigo requere se solicite y almacene.';
COMMENT ON COLUMN unico.di_pagos.dato2 IS 'Nombre de dato adicional 2';
COMMENT ON COLUMN unico.di_pagos.dato3 IS 'Nombre de dato adicional 3';
COMMENT ON COLUMN unico.di_pagos.dato4 IS 'Nombre de dato adicional 4';
COMMENT ON COLUMN unico.di_pagos.dato5 IS 'Nombre de dato adicional 5';
COMMENT ON COLUMN unico.di_pagos.dmlnombre IS 'Query para obtener el nombre del cliente de la tabla de deudas correspondiente.';
COMMENT ON COLUMN unico.di_pagos.dmlselect IS 'Consulta que debe retornar los campos de deuda, detalle (opcional) y periodo (opcional).';
COMMENT ON COLUMN unico.di_pagos.estado IS 'Estado del pago de deudas ON, OF';
COMMENT ON COLUMN unico.di_pagos.funpago_adelanto IS 'Funcion PL que se invocar� si el pago es ADE';
COMMENT ON COLUMN unico.di_pagos.funpago_parcial IS 'Funcion PL que se invocar� si el pago es PAR';
COMMENT ON COLUMN unico.di_pagos.funpago_periodo IS 'Funcion PL que se invocar� si el pago es por periodos (falta implementar esta logica).';
COMMENT ON COLUMN unico.di_pagos.funpago_total IS 'Funcion PL que se invocar� si el pago es TOT';
COMMENT ON COLUMN unico.di_pagos.id_pago IS 'ID de la configuracion de pagos.';
COMMENT ON COLUMN unico.di_pagos.moneda IS 'Sigla de moneda. pe. BOB, USD';
COMMENT ON COLUMN unico.di_pagos.pago_maximo IS 'Monto maximo del pago (para uso posterior, no esta implementado)';
COMMENT ON COLUMN unico.di_pagos.pago_minimo IS 'Monto minimo del pago (para uso posterior, no esta implementado)';
COMMENT ON COLUMN unico.di_pagos.tipo IS 'Tipo de pago: Total (TOT), Parcial (PAR), Adelantado (ADE)';


CREATE SEQUENCE unico.sec_id_pago
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 999999999
  NOCYCLE
  NOORDER
  CACHE 20;
/

/*******************************************************************************
di_claves
*******************************************************************************/

CREATE TABLE unico.di_claves
    (id_pago                        NUMBER(6,0) NOT NULL,
    clave                          VARCHAR2(15) NOT NULL,
    descripcion                    VARCHAR2(50) NOT NULL,
    entrada_pago                   NUMBER(1,0) NOT NULL,
    entrada_consulta               NUMBER(1,0) NOT NULL);

ALTER TABLE unico.di_claves
ADD CONSTRAINT ck_clave UNIQUE (id_pago, clave)
USING INDEX;

COMMENT ON COLUMN unico.di_claves.clave IS 'Codigo de la clave que se utilizara en la configuracion de expresiones relacionadas con la base de datos, pe. cms, ci, nit, etc.';
COMMENT ON COLUMN unico.di_claves.descripcion IS 'Descripcion del dato de entrada.';
COMMENT ON COLUMN unico.di_claves.entrada_consulta IS 'Indica si la clave debe pedirse en el metodo de consulta de deudas.';
COMMENT ON COLUMN unico.di_claves.entrada_pago IS 'Indica si la clave debe pedirse en el metodo de pago de deudas.';
COMMENT ON COLUMN unico.di_claves.id_pago IS 'ID unico de la configuracion del pago, este valor debe gerenarse con la secuenia respectiva.';

ALTER TABLE unico.di_claves
ADD CONSTRAINT fk_clave FOREIGN KEY (id_pago)
REFERENCES unico.di_pagos (id_pago);
/

/*******************************************************************************
tx_exportacion
*******************************************************************************/

CREATE TABLE unico.tx_exportacion
    (id_exportacion                 NUMBER(9,0) NOT NULL,
    nombre_archivo                 VARCHAR2(50) NOT NULL,
    registros                      NUMBER(9,0) NOT NULL,
    fecha_inicio                   DATE NOT NULL,
    fecha_final                    DATE NOT NULL,
    sq_exportacion                 NUMBER(9,0) NOT NULL);


COMMENT ON COLUMN unico.tx_exportacion.fecha_final IS 'Hora final del proceso.';
COMMENT ON COLUMN unico.tx_exportacion.fecha_inicio IS 'Hora inicio del proceso.';
COMMENT ON COLUMN unico.tx_exportacion.id_exportacion IS 'ID de relacion con la tabla de configuracion de exportacion de pagos.';
COMMENT ON COLUMN unico.tx_exportacion.nombre_archivo IS 'Nombre de archivo salida.';
COMMENT ON COLUMN unico.tx_exportacion.registros IS 'Numero de registros exportados.';
COMMENT ON COLUMN unico.tx_exportacion.sq_exportacion IS 'Secuencia de la exportacion, toma su valor de la secuencia respectiva.';

ALTER TABLE unico.tx_exportacion
ADD CONSTRAINT fk_exportacion FOREIGN KEY (id_exportacion)
REFERENCES UNICO.di_exportacion (id_exportacion);

CREATE SEQUENCE unico.sec_exportacion
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 999999999
  NOCYCLE
  NOORDER
  CACHE 20;
/

/*******************************************************************************
tx_importacion
*******************************************************************************/
    
CREATE TABLE unico.tx_importacion
    (id_importacion                 NUMBER(9,0) NOT NULL,
    fecha_inicio                   DATE NOT NULL,
    nombre_archivo                 VARCHAR2(50) NOT NULL,
    registros                      NUMBER(9,0) NOT NULL,
    fecha_final                    DATE NOT NULL,
    sq_importacion                 NUMBER(9,0) NOT NULL,
    nombre_paquete                 VARCHAR2(50),
    estado                         CHAR(3) NOT NULL);

ALTER TABLE unico.tx_importacion
ADD CONSTRAINT ck_estado_importacion CHECK (estado in ('SUC','ERR'));

COMMENT ON COLUMN unico.tx_importacion.estado IS 'Indica el estado de la importacion: SUC=exitoso, ERR=Con errores';
COMMENT ON COLUMN unico.tx_importacion.fecha_final IS 'Fecha final de la carga';
COMMENT ON COLUMN unico.tx_importacion.fecha_inicio IS 'Fecha de la importacion de datos del archivo.';
COMMENT ON COLUMN unico.tx_importacion.id_importacion IS 'ID de relacion con la tabla de configuracion de importacion';
COMMENT ON COLUMN unico.tx_importacion.nombre_archivo IS 'Nombre del archivo importado';
COMMENT ON COLUMN unico.tx_importacion.nombre_paquete IS 'Nombre del archivo del archivo zip origen, si esta configurado ZIP como tipo de archivo de entrada.';
COMMENT ON COLUMN unico.tx_importacion.registros IS 'Numero de registros registrados';
COMMENT ON COLUMN unico.tx_importacion.sq_importacion IS 'Secuencia de la importacion, toma su valor de la secuencia asociada.';


ALTER TABLE unico.tx_importacion
ADD CONSTRAINT fk_importacion FOREIGN KEY (id_importacion)
REFERENCES UNICO.di_importacion (id_importacion) ON DELETE CASCADE;

CREATE SEQUENCE unico.sec_importacion
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 999999999
  NOCYCLE
  NOORDER
  CACHE 20;
/

/*******************************************************************************
tx_pagos
*******************************************************************************/
    
CREATE TABLE unico.tx_pagos
    (id_pago                        NUMBER(9,0) NOT NULL,
    monto                          NUMBER(8,2) DEFAULT 0 NOT NULL,
    fecha_pago                     DATE NOT NULL,
    nro_cliente                    VARCHAR2(15) NOT NULL,
    nro_ptm                        VARCHAR2(15),
    nro_extra                      VARCHAR2(15),
    estado                         CHAR(3) DEFAULT NULL NOT NULL,
    sq_exportacion                 NUMBER(9,0) DEFAULT 0 NOT NULL,
    clave1                         VARCHAR2(10) NOT NULL,
    cuenta                         VARCHAR2(15) NOT NULL,
    dato1                          VARCHAR2(50),
    dato2                          VARCHAR2(50),
    dato3                          VARCHAR2(50),
    dato4                          VARCHAR2(50),
    dato5                          VARCHAR2(50),
    clave2                         VARCHAR2(10),
    clave3                         VARCHAR2(10),
    deuda_inicial                  NUMBER(8,2) NOT NULL,
    tipo_cambio                    NUMBER(8,2) NOT NULL);


ALTER TABLE unico.tx_pagos
ADD CONSTRAINT ck_estado_pago CHECK (ESTADO IN ('NEW','ERR','EXP'));


COMMENT ON COLUMN unico.tx_pagos.clave1 IS 'Clave 1 del cliente, pe. cns (codigo de promotora)';
COMMENT ON COLUMN unico.tx_pagos.clave2 IS 'Clave 2 del cliente, pe. ciudad (codigo de ciudad)';
COMMENT ON COLUMN unico.tx_pagos.clave3 IS 'Clave 3 del cliente';
COMMENT ON COLUMN unico.tx_pagos.cuenta IS 'Numero de cuanta bancaria, pe.653767770';
COMMENT ON COLUMN unico.tx_pagos.dato1 IS 'Dato adicional que sera registrado, pe. nit (numero de nit)';
COMMENT ON COLUMN unico.tx_pagos.dato2 IS 'Dato adicional que sera registrado';
COMMENT ON COLUMN unico.tx_pagos.dato3 IS 'Dato adicional que sera registrado';
COMMENT ON COLUMN unico.tx_pagos.dato4 IS 'Dato adicional que sera registrado';
COMMENT ON COLUMN unico.tx_pagos.dato5 IS 'Dato adicional que sera registrado';
COMMENT ON COLUMN unico.tx_pagos.deuda_inicial IS 'Deuda que tenia antes de realizar el pago.';
COMMENT ON COLUMN unico.tx_pagos.estado IS 'Estado NEW=nuevo,ERR=error,EXP=exportado';
COMMENT ON COLUMN unico.tx_pagos.fecha_pago IS 'Fecha del pago.';
COMMENT ON COLUMN unico.tx_pagos.id_pago IS 'ID relacionado con la tabla de configuracion del pago.';
COMMENT ON COLUMN unico.tx_pagos.monto IS 'Monto del pago';
COMMENT ON COLUMN unico.tx_pagos.nro_cliente IS 'Numero telefono del cliente.';
COMMENT ON COLUMN unico.tx_pagos.nro_extra IS 'Numero adicional (para uso futuro)';
COMMENT ON COLUMN unico.tx_pagos.nro_ptm IS 'Numero PTM.';
COMMENT ON COLUMN unico.tx_pagos.sq_exportacion IS 'Secuencia del archivo en que fue exportado el pago, el valor 0 indica que no fue exportado.';
COMMENT ON COLUMN unico.tx_pagos.tipo_cambio IS 'Tipo de cambio con el cual se hizo el pago, si es 1 indica un debito en bolivianos.';


ALTER TABLE unico.tx_pagos
ADD CONSTRAINT fk_pago FOREIGN KEY (id_pago)
REFERENCES UNICO.di_pagos (id_pago);
/

/*******************************************************************************
tx_pagos_error
*******************************************************************************/

CREATE TABLE unico.tx_pagos_error
    (cod_servicio                   VARCHAR2(20),
    monto                          NUMBER(9,2),
    fecha_pago                     DATE,
    nro_cliente                    VARCHAR2(50),
    nro_ptm                        VARCHAR2(50),
    dato1                          VARCHAR2(50),
    dato2                          VARCHAR2(50),
    dato3                          VARCHAR2(50),
    dato4                          VARCHAR2(50),
    dato5                          VARCHAR2(50),
    valor1                         VARCHAR2(50),
    valor2                         VARCHAR2(50),
    valor3                         VARCHAR2(50),
    valor4                         VARCHAR2(50),
    valor5                         VARCHAR2(50),
    error                          VARCHAR2(250));
    
COMMENT ON COLUMN unico.tx_pagos_error.cod_servicio IS 'ID relacion con la tabla BC_SERVICIO';
COMMENT ON COLUMN unico.tx_pagos_error.dato1 IS 'Nombre del dato incluido en el pago';
COMMENT ON COLUMN unico.tx_pagos_error.dato2 IS 'Nombre del dato incluido en el pago';
COMMENT ON COLUMN unico.tx_pagos_error.dato3 IS 'Nombre del dato incluido en el pago';
COMMENT ON COLUMN unico.tx_pagos_error.dato4 IS 'Nombre del dato incluido en el pago';
COMMENT ON COLUMN unico.tx_pagos_error.dato5 IS 'Nombre del dato incluido en el pago';
COMMENT ON COLUMN unico.tx_pagos_error.error IS 'Descripcion del error ocurrido.';
COMMENT ON COLUMN unico.tx_pagos_error.fecha_pago IS 'Fecha del pago';
COMMENT ON COLUMN unico.tx_pagos_error.monto IS 'Monto del pago';
COMMENT ON COLUMN unico.tx_pagos_error.nro_cliente IS 'Numero del cliente.';
COMMENT ON COLUMN unico.tx_pagos_error.nro_ptm IS 'Numero PTM';
COMMENT ON COLUMN unico.tx_pagos_error.valor1 IS 'Valor del dato incluido en el pago';
COMMENT ON COLUMN unico.tx_pagos_error.valor2 IS 'Valor del dato incluido en el pago';
COMMENT ON COLUMN unico.tx_pagos_error.valor3 IS 'Valor del dato incluido en el pago';
COMMENT ON COLUMN unico.tx_pagos_error.valor4 IS 'Valor del dato incluido en el pago';
COMMENT ON COLUMN unico.tx_pagos_error.valor5 IS 'Valor del dato incluido en el pago';


/*******************************************************************************
CONFIGURACION ESPECIFICA PARA YANBAL
*******************************************************************************/


/*******************************************************************************
tx_yanbal
*******************************************************************************/
    
CREATE TABLE unico.tx_yanbal
    (cns                            NUMBER(15,0) NOT NULL,
    deuda                          NUMBER(9,2) DEFAULT 0 NOT NULL,
    nro_cuenta                     VARCHAR2(15),
    nombre                         VARCHAR2(60));

CREATE INDEX unico.dx_cns_yanbal ON unico.tx_yanbal(
    cns                             ASC);

ALTER TABLE unico.tx_yanbal
ADD CONSTRAINT pk_yanbal PRIMARY KEY (cns, nro_cuenta)
USING INDEX;

COMMENT ON COLUMN unico.tx_yanbal.cns IS 'Codigo de promotora';
COMMENT ON COLUMN unico.tx_yanbal.deuda IS 'Deuda';
COMMENT ON COLUMN unico.tx_yanbal.nombre IS 'Nombre de la promotora';
COMMENT ON COLUMN unico.tx_yanbal.nro_cuenta IS 'Numero de cuenta: 4010599394 para Sus y 4010538224 para Bs';
/

/*******************************************************************************
configuracion inicial
*******************************************************************************/
DECLARE
    ----------------------------------------------------------------------------
    -- parametros a modificar
    ----------------------------------------------------------------------------
    -- RUTA DEL DIRECTORIO LOCAL PARA ARCHIVOS TEMPORALES A CARGAR
    ----------------------------------------------------------------------------
    varRutaLocalTmpImp VARCHAR2(200):='/unico/colecturia/importacion/';  
    ----------------------------------------------------------------------------
    -- RUTA DEL DIRECTORIO LOCAL PARA ARCHIVOS TEMPORALES A EXPORTAR
    ----------------------------------------------------------------------------
    varRutaLocalTmpExp VARCHAR2(200):='/unico/colecturia/exportacion/';
    ----------------------------------------------------------------------------
    -- parametros fijos
    ----------------------------------------------------------------------------
    varCodServicio VARCHAR2(50):='64';
    varFtpHost VARCHAR2(50):='10.250.0.14';
    varFtpPort VARCHAR2(50):='21';
    varFtpUsername VARCHAR2(50):='fflores';
    varFtpPassword VARCHAR2(50):='fflores';
    varFtpRutaEntrada VARCHAR2(50):='/YANBAL/deudas_entrada';
    varFtpRutaSalida VARCHAR2(50):='/YANBAL/deudas_salida';
    varFtpRutaRechazado VARCHAR2(50):='/YANBAL/deudas_rechazada';
    varFtpRutaReporte VARCHAR2(50):='/YANBAL/pagos_salida';
    ----------------------------------------------------------------------------
    -- variables
    ----------------------------------------------------------------------------
    varIdConexion NUMBER(9,0);
    varIdPago NUMBER(9,0);
    varIdEmail NUMBER(9,0);
BEGIN

    BEGIN    
        SELECT s.cod_servicio INTO varCodServicio FROM bc_empresa a, bc_servicio s WHERE rownum<=1 AND a.cod_empresa=s.cod_empresa AND a.descripcion='YANBAL';
        dbms_output.put_line('Codigo servicio obtenido');
    EXCEPTION WHEN others THEN 
        dbms_output.put_line('ERROR: No pudo obtenerse el codigo servicio.');
        RETURN;
    END;
    
    SELECT unico.sec_id_conexion.nextval INTO varIdConexion FROM DUAL;
    INSERT INTO unico.di_conexion (ID_CONEXION,NOMBRE,PROTOCOLO)
    VALUES(varIdConexion,'Colecturia de deudas','FTP');
    COMMIT;
    dbms_output.put_line('conexion creado');

    INSERT INTO unico.di_atributo_conexion (ID_CONEXION,NOMBRE,VALOR,ENCRIPTADO)
    VALUES(varIdConexion,'port',varFtpPort,0);
    INSERT INTO unico.di_atributo_conexion (ID_CONEXION,NOMBRE,VALOR,ENCRIPTADO)
    VALUES(varIdConexion,'host',varFtpHost,0);
    INSERT INTO unico.di_atributo_conexion (ID_CONEXION,NOMBRE,VALOR,ENCRIPTADO)
    VALUES(varIdConexion,'username',varFtpUsername,0);
    INSERT INTO unico.di_atributo_conexion (ID_CONEXION,NOMBRE,VALOR,ENCRIPTADO)
    VALUES(varIdConexion,'password',varFtpPassword,0);
    COMMIT;
    dbms_output.put_line('atributos creado');


    INSERT INTO di_importacion (ID_IMPORTACION,COD_SERVICIO,ID_CONEXION,ESTADO,SEPARADOR,EXTENSION_ENTRADA,DIRECTORIO_ENTRADA,EXTENSION_SALIDA,DIRECTORIO_SALIDA,LINEA_INICIAL,DMLDELETE,DMLINSERT,EXTENSION_RECHAZO,DIRECTORIO_RECHAZO,DMLUPDATE,PREFIJO_ENTRADA,ORDENACION,DIRECTORIO_TEMPORAL,TIPO_ENTRADA,EXTENSION_UNZIP,PREFIJO_UNZIP,DMLINICIAL,BLOQUE_TRANSACCION,LIMITE_ERRORES,EXTENSION_PROCESO,DESCRIPCION)
    VALUES(unico.sec_id_importacion.nextval,varCodServicio,varIdConexion,'ON',',','.DN2',varFtpRutaEntrada,'.done',varFtpRutaSalida,1,NULL,'insert into TX_YANBAL values ({1},pck_yanbal.deuda(''{2}''),{3},pck_yanbal.nombre(''{2}''))','.err',varFtpRutaRechazado,NULL,'YANB',2,varRutaLocalTmpImp,'TXT',NULL,NULL,'delete from TX_YANBAL where nro_cuenta=''4010538224''',1000,0,'.pro','Importacion deudas en Sus Yanbal');
    INSERT INTO di_importacion (ID_IMPORTACION,COD_SERVICIO,ID_CONEXION,ESTADO,SEPARADOR,EXTENSION_ENTRADA,DIRECTORIO_ENTRADA,EXTENSION_SALIDA,DIRECTORIO_SALIDA,LINEA_INICIAL,DMLDELETE,DMLINSERT,EXTENSION_RECHAZO,DIRECTORIO_RECHAZO,DMLUPDATE,PREFIJO_ENTRADA,ORDENACION,DIRECTORIO_TEMPORAL,TIPO_ENTRADA,EXTENSION_UNZIP,PREFIJO_UNZIP,DMLINICIAL,BLOQUE_TRANSACCION,LIMITE_ERRORES,EXTENSION_PROCESO,DESCRIPCION)
    VALUES(unico.sec_id_importacion.nextval,varCodServicio,varIdConexion,'ON',',','.DN2',varFtpRutaEntrada,'.done',varFtpRutaSalida,1,NULL,'insert into TX_YANBAL values ({1},pck_yanbal.deuda(''{2}''),{3},pck_yanbal.nombre(''{2}''))','.err',varFtpRutaRechazado,NULL,'YAND',2,varRutaLocalTmpImp,'TXT',NULL,NULL,'delete from TX_YANBAL where nro_cuenta=''4010599394''',1000,0,'.pro','Importacion deudas en Bs Yanbal');
    COMMIT;
    dbms_output.put_line('importacion creado');


    INSERT INTO di_exportacion (ID_EXPORTACION,COD_SERVICIO,ID_CONEXION,ESTADO,EXTENSION_SALIDA,DIRECTORIO_SALIDA,SEPARADOR,DIRECTORIO_TEMPORAL,TIPO_SALIDA,QUERY_DATOS,QUERY_NOMBRE,FILTRO_CUENTA,EXTENSION_PROCESO,DESCRIPCION)
    VALUES(unico.sec_id_exportacion.nextval,varCodServicio,varIdConexion,'OF',NULL,varFtpRutaReporte,'\t',varRutaLocalTmpExp,'TXT','SELECT to_char(fecha_pago,''DDMMYYYY'')||''|''||clave1||''|''||to_char(fecha_pago,''HH24:MI:SS'')||''|||''||replace(trim(to_char(monto,''9999999.00'')),''.'','''')||''||||'' AS P1 FROM tx_pagos','select ''CC4010599394.''||to_char(sysdate,''MMDD.HH24MISS'') from dual','4010599394','.par','Exportacion pagos dolares Yanbal');
    INSERT INTO di_exportacion (ID_EXPORTACION,COD_SERVICIO,ID_CONEXION,ESTADO,EXTENSION_SALIDA,DIRECTORIO_SALIDA,SEPARADOR,DIRECTORIO_TEMPORAL,TIPO_SALIDA,QUERY_DATOS,QUERY_NOMBRE,FILTRO_CUENTA,EXTENSION_PROCESO,DESCRIPCION)
    VALUES(unico.sec_id_exportacion.nextval,varCodServicio,varIdConexion,'ON',NULL,varFtpRutaReporte,'\t',varRutaLocalTmpExp,'TXT','SELECT to_char(fecha_pago,''DDMMYYYY'')||''|''||clave1||''|''||to_char(fecha_pago,''HH24:MI:SS'')||''|||''||replace(trim(to_char(monto,''9999999.00'')),''.'','''')||''||||'' AS P1 FROM tx_pagos','select ''CC4010538224.''||to_char(sysdate,''MMDD.HH24MISS'') from dual','4010538224','.par','Exportacion pagos bolivianos Yanbal');
    COMMIT;
    dbms_output.put_line('exportacion creado');


    SELECT unico.sec_id_pago.nextval INTO varIdPago FROM DUAL;
    INSERT INTO di_pagos (ID_PAGO,COD_SERVICIO,DMLSELECT,TIPO,ESTADO,MONEDA,FUNPAGO_TOTAL,FUNPAGO_PARCIAL,FUNPAGO_ADELANTO,FUNPAGO_PERIODO,DMLNOMBRE,PAGO_MINIMO,PAGO_MAXIMO,DATO1,DATO2,DATO3,DATO4,DATO5)
    VALUES(varIdPago,varCodServicio,'select deuda,decode(nro_cuenta,''4010538224'',''BOB'',''4010599394'',''USD'',null) from tx_yanbal where cns>100 and cns={cns}','ADE','ON','BOB',NULL,NULL,'pck_yanbal.pago_adelantado({cns},{$})',NULL,'select nombre from tx_yanbal where rownum=1 and cns={cns}',0,0,NULL,NULL,NULL,NULL,NULL);
    COMMIT;
    dbms_output.put_line('pagos creado');
    
    INSERT INTO di_claves (ID_PAGO,CLAVE,DESCRIPCION,ENTRADA_PAGO,ENTRADA_CONSULTA)
    VALUES(varIdPago,'cns','Numero de promotora',1,1);
    COMMIT;
    dbms_output.put_line('parametros de pagos creado');
    
    
    SELECT unico.sec_id_email.nextval INTO varIdEmail FROM DUAL;
    INSERT INTO di_email (ID_EMAIL,DIRECCION)
    VALUES(varIdEmail,'fflores@mc4.com.bo');
    COMMIT;
    INSERT INTO di_email_servicio (ID_EMAIL,COD_SERVICIO,ALERTA_ERRORES,ALERTA_EVENTOS,ESTADO)
    VALUES(varIdEmail,varCodServicio,0,0,'OF');
    COMMIT;
    dbms_output.put_line('email creado');
    
    
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140801);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140802);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140803);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140804);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140805);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140806);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140807);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140808);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140809);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140810);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140811);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140812);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140813);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140814);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140815);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140816);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140817);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140818);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140819);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140820);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140821);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140822);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140823);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140824);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140825);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140826);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140827);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140828);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140829);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140830);
    INSERT INTO di_tipo_cambio (MONEDA,CAMBIO,FECHA)
    VALUES('USD',6.96,140831);
    COMMIT;
    dbms_output.put_line('tipo de cambio creado');
    
    
    INSERT INTO tx_yanbal (CNS,DEUDA,NRO_CUENTA,NOMBRE)
    VALUES(4622703,523,'4010538224','SILVIO ROJAS DE PACA');
    INSERT INTO tx_yanbal (CNS,DEUDA,NRO_CUENTA,NOMBRE)
    VALUES(4622714,293,'4010538224','ESTHER GARCIA');
    INSERT INTO tx_yanbal (CNS,DEUDA,NRO_CUENTA,NOMBRE)
    VALUES(4622725,0,'4010538224','JANETH PACOTILLA ROJAS');
    INSERT INTO tx_yanbal (CNS,DEUDA,NRO_CUENTA,NOMBRE)
    VALUES(4622736,55.90,'4010538224','MILENA GOMEZ RODRIGUEZ');
    INSERT INTO tx_yanbal (CNS,DEUDA,NRO_CUENTA,NOMBRE)
    VALUES(4622747,10,'4010538224','EVELYN MARTHA RAMSIS');
    INSERT INTO tx_yanbal (CNS,DEUDA,NRO_CUENTA,NOMBRE)
    VALUES(4622758,1090.80,'4010538224','FAVIOLA RUTH LICITO');
    INSERT INTO tx_yanbal (CNS,DEUDA,NRO_CUENTA,NOMBRE)
    VALUES(4622703,0,'4010599394','SILVIO ROJAS DE PACA');
    INSERT INTO tx_yanbal (CNS,DEUDA,NRO_CUENTA,NOMBRE)
    VALUES(4622714,5,'4010599394','ESTHER GARCIA');
    INSERT INTO tx_yanbal (CNS,DEUDA,NRO_CUENTA,NOMBRE)
    VALUES(4622725,10.5,'4010599394','JANETH PACOTILLA ROJAS');
    INSERT INTO tx_yanbal (CNS,DEUDA,NRO_CUENTA,NOMBRE)
    VALUES(4622736,0,'4010599394','MILENA GOMEZ RODRIGUEZ');
    INSERT INTO tx_yanbal (CNS,DEUDA,NRO_CUENTA,NOMBRE)
    VALUES(4622747,80.10,'4010599394','EVELYN MARTHA RAMSIS');
    INSERT INTO tx_yanbal (CNS,DEUDA,NRO_CUENTA,NOMBRE)
    VALUES(4622758,80,'4010599394','FAVIOLA RUTH LICITO');
    COMMIT;
    dbms_output.put_line('ejemplos creado');
    
    
    dbms_output.put_line('OK');
    
EXCEPTION
    WHEN others THEN
    ROLLBACK;
    dbms_output.put_line('EXCEPTION:'||SQLERRM);
END;
/

/*******************************************************************************
pck_yanbal
*******************************************************************************/

CREATE OR REPLACE 
PACKAGE unico.pck_yanbal
IS

FUNCTION deuda(
    columna IN VARCHAR2
)RETURN NUMBER;

FUNCTION nombre(
    columna IN VARCHAR2
)RETURN VARCHAR2;

FUNCTION pago_total(
    id IN tx_yanbal.cns%type
)RETURN VARCHAR2;

FUNCTION pago_parcial(
    id IN tx_yanbal.cns%type, 
    in_pago IN NUMBER
)RETURN VARCHAR2;

FUNCTION pago_adelantado(
    id IN tx_yanbal.cns%type, 
    in_pago IN NUMBER
)RETURN VARCHAR2;

FUNCTION pago_periodo(
    id IN tx_yanbal.cns%type, 
    periodo IN VARCHAR2
)RETURN VARCHAR2;

END pck_yanbal; -- Package spec
/

CREATE OR REPLACE 
PACKAGE BODY unico.pck_yanbal
IS

--//////////////////////////////////////////////////////////////////////////////
--Funcion para obtener la deuda
--//////////////////////////////////////////////////////////////////////////////
FUNCTION deuda(
    columna IN VARCHAR2
)RETURN NUMBER IS
    expresion VARCHAR2(50);
    monto1 VARCHAR2(9);
    monto2 VARCHAR2(20);
    k PLS_INTEGER;
BEGIN
    expresion := SUBSTR(columna,0,LENGTH(columna)-4);
    k := INSTR(expresion,' ',-1);
    IF k<1 THEN
        RETURN expresion;
    END IF;
    monto1 := substr(expresion,k+1);
    expresion := trim(substr(expresion,0,k));
    k := instr(expresion,' ',-1);
    monto2 := substr(expresion,k+1);
    IF trim(translate(monto2,'0123456789',' ')) is null THEN
        RETURN monto2||'.'||monto1;
    ELSE
        RETURN monto1;
    END IF;
EXCEPTION
    WHEN others THEN
    RETURN 0;
END deuda;


--//////////////////////////////////////////////////////////////////////////////
--Funcion para obtener el nombre de la promotora
--//////////////////////////////////////////////////////////////////////////////
FUNCTION nombre(
    columna IN VARCHAR2
)RETURN VARCHAR2 IS
    k PLS_INTEGER;
    expresion VARCHAR2(50);
    monto1 VARCHAR2(20);
BEGIN 
    expresion := rtrim(substr(columna,0,LENGTH(columna)-4));
    k := instr(expresion,' ',-1); 
    IF k=0 THEN
        RETURN '';
    END IF;
    monto1:=substr(expresion,k+1,1);
    expresion := rtrim(substr(expresion,1,k-1));
    k := instr(expresion,' ',-1); 
    monto1:=substr(expresion,k+1,1);
    IF not trim(translate(monto1,'0123456789',' ')) is null THEN
        RETURN expresion;
    END IF;
    expresion := rtrim(substr(expresion,1,k-1));
    RETURN expresion;
EXCEPTION
    WHEN others THEN
    RETURN '';
END nombre;

--//////////////////////////////////////////////////////////////////////////////
--Funcion para pagar el total de la deuda
--//////////////////////////////////////////////////////////////////////////////
FUNCTION pago_total(
    id IN tx_yanbal.cns%type
)RETURN VARCHAR2 IS
    --PRAGMA AUTONOMOUS_TRANSACTION;
    --varMonto tx_yanbal.deuda%type;
BEGIN
    RETURN 'Esta operacion no esta soportada.';
    /*
    SELECT sum(deuda) INTO varMonto FROM tx_yanbal WHERE cns=id;
    IF varMonto IS NULL THEN
        RETURN 'No se encontraron deudas.';
    END IF;
    UPDATE tx_yanbal SET deuda=0 WHERE cns=id;
    COMMIT;
    RETURN 'OK';
EXCEPTION
    WHEN others THEN
    ROLLBACK;
    RETURN SQLERRM;
    */
END pago_total;

--//////////////////////////////////////////////////////////////////////////////
--Funcion para pagar parcialmente la deuda
--//////////////////////////////////////////////////////////////////////////////
FUNCTION pago_parcial(
    id IN tx_yanbal.cns%type, 
    in_pago IN NUMBER
)RETURN VARCHAR2 IS
    --PRAGMA AUTONOMOUS_TRANSACTION;
    --varMonto tx_yanbal.deuda%type;
BEGIN
    RETURN 'Esta operacion no esta soportada.';
    /*
    SELECT sum(deuda) INTO varMonto FROM tx_yanbal WHERE cns=id;
    IF varMonto IS NULL THEN
        RETURN 'No se encontraron deudas.';
    END IF;
    IF FLOOR(varMonto) < ROUND(in_pago) THEN
        RETURN 'El monto pagado es mayor al adeudado: '||to_char(varMonto);
    END IF;
    UPDATE tx_yanbal SET deuda=deuda-in_pago WHERE cns=id;
    COMMIT;
    RETURN 'OK';
EXCEPTION
    WHEN others THEN
    ROLLBACK;
    RETURN SQLERRM;
    */
END pago_parcial;

--//////////////////////////////////////////////////////////////////////////////
--Funcion para pagar el total, meno o adelantar un monto
--Formato que debe retornar esta funcion: OK;<deuda>;<pago:cuenta:cambio>
--//////////////////////////////////////////////////////////////////////////////
FUNCTION pago_adelantado(
    id IN tx_yanbal.cns%type, 
    in_pago IN NUMBER
)RETURN VARCHAR2 IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    cteCuentaBob CONSTANT VARCHAR2(10) := '4010538224';
    cteCuentaUsd CONSTANT VARCHAR2(10) := '4010599394';
    varDeudaBob tx_yanbal.deuda%type;
    varDeudaUsd tx_yanbal.deuda%type;
    varDeudaTotal tx_yanbal.deuda%type;
    varSaldo tx_yanbal.deuda%type;
    varTipoDeCambio di_tipo_cambio.cambio%type;
    ----------------------------------------------------------------------------
    FUNCTION ftmMoneda(
        valor tx_yanbal.deuda%type
    ) RETURN VARCHAR2 IS
    BEGIN
        RETURN trim(to_char(valor,'9999999.99'));
    END;
    ----------------------------------------------------------------------------
    FUNCTION getTipoCambio
    RETURN di_tipo_cambio.cambio%type IS
        varTipoDeCambio di_tipo_cambio.cambio%type;
    BEGIN
        SELECT cambio INTO varTipoDeCambio FROM di_tipo_cambio WHERE fecha=to_number(to_char(SYSDATE,'YYMMDD'));
        RETURN varTipoDeCambio;
    EXCEPTION WHEN NO_DATA_FOUND THEN
        BEGIN
            SELECT cambio INTO varTipoDeCambio FROM di_tipo_cambio WHERE fecha=to_number(to_char(SYSDATE-1,'YYMMDD'));
            RETURN varTipoDeCambio;
        EXCEPTION WHEN NO_DATA_FOUND THEN
            SELECT cambio INTO varTipoDeCambio FROM di_tipo_cambio WHERE fecha IN (SELECT max(fecha) FROM di_tipo_cambio);
            RETURN varTipoDeCambio;
        END;
    END;
BEGIN
    SELECT sum(deuda) INTO varDeudaBob FROM tx_yanbal WHERE nro_cuenta=cteCuentaBob AND cns=id;
    SELECT sum(deuda) INTO varDeudaUsd FROM tx_yanbal WHERE nro_cuenta=cteCuentaUsd AND cns=id;
    IF varDeudaBob IS NULL THEN
        IF varDeudaUsd IS NULL THEN
            --------------------------------------------------------------------
            -- no se encontro registro
            --------------------------------------------------------------------
            RETURN 'El promotor con CNS '||id||' no existe.';
        ELSE
            --------------------------------------------------------------------
            -- tiene deuda en dolares
            --------------------------------------------------------------------
            varTipoDeCambio := getTipoCambio();
            varDeudaTotal := varDeudaUsd * varTipoDeCambio;
            --------------------------------------------------------------------
            -- si la deuda es mayor al pago -> debitar su deuda en dolares
            --------------------------------------------------------------------
            IF varDeudaTotal > in_pago THEN
                UPDATE tx_yanbal SET deuda=deuda-(in_pago/varTipoDeCambio) WHERE nro_cuenta=cteCuentaUsd AND cns=id;
                COMMIT;
            --------------------------------------------------------------------
            -- si la deuda es menor al pago -> poner deuda cero en dolares y abonar un adelanto en bolivianos
            --------------------------------------------------------------------           
            ELSE
                UPDATE tx_yanbal SET deuda=0 WHERE nro_cuenta=cteCuentaUsd AND cns=id;
                INSERT INTO tx_yanbal (cns,deuda,nro_cuenta,nombre) VALUES (id,in_pago-varDeudaTotal,cteCuentaBob,null);
                COMMIT;
            END IF;
            RETURN 'OK;'||ftmMoneda(varDeudaTotal)||';'||ftmMoneda(in_pago)||':'||cteCuentaBob||':'||ftmMoneda(varTipoDeCambio); 
        END IF;
    ELSE
        IF varDeudaUsd IS NULL THEN
            --------------------------------------------------------------------
            -- solo tiene deuda en bolivianos
            --------------------------------------------------------------------
            UPDATE tx_yanbal SET deuda=deuda-in_pago WHERE nro_cuenta=cteCuentaBob AND cns=id;
            COMMIT;            
            RETURN 'OK;'||ftmMoneda(varDeudaBob)||';'||ftmMoneda(in_pago)||':'||cteCuentaBob||':1'; 
        ELSE
            --------------------------------------------------------------------
            -- tiene deuda en ambas cuentas
            --------------------------------------------------------------------
            varTipoDeCambio := getTipoCambio();
            varDeudaTotal := varDeudaUsd * varTipoDeCambio;
            varDeudaTotal := varDeudaTotal + varDeudaBob;
            --------------------------------------------------------------------
            -- si la deuda en bs. es mayor al pago -> debitar de cuenta en bs. 
            --------------------------------------------------------------------
            IF varDeudaBob > in_pago THEN
                UPDATE tx_yanbal SET deuda=deuda-in_pago WHERE nro_cuenta=cteCuentaBob AND cns=id;
                COMMIT;          
                RETURN 'OK;'||ftmMoneda(varDeudaTotal)||';'||ftmMoneda(in_pago)||':'||cteCuentaBob||':1'; 
            --------------------------------------------------------------------
            -- si la deuda en bs. es menor al pago -> poner la cuenta bs en cero
            --------------------------------------------------------------------
            ELSE
                                            
                IF varDeudaUsd*varTipoDeCambio > in_pago-varDeudaBob THEN
                    UPDATE tx_yanbal SET deuda = 0 WHERE nro_cuenta=cteCuentaBob AND cns=id;
                    UPDATE tx_yanbal SET deuda = deuda-((in_pago-varDeudaBob)/varTipoDeCambio) WHERE nro_cuenta=cteCuentaUsd AND cns=id;
                    COMMIT;                   
                ELSE
                    UPDATE tx_yanbal SET deuda = 0 WHERE nro_cuenta=cteCuentaUsd AND cns=id;
                    UPDATE tx_yanbal SET deuda = varDeudaBob - (in_pago-(varDeudaUsd*varTipoDeCambio)) WHERE nro_cuenta=cteCuentaBob AND cns=id;
                    COMMIT;
                END IF;
                RETURN 'OK;'||ftmMoneda(varDeudaTotal)||';'||ftmMoneda(in_pago)||':'||cteCuentaBob||':' ||ftmMoneda(varTipoDeCambio); 
            END IF;
        END IF;
    END IF;
EXCEPTION
    WHEN others THEN
    ROLLBACK;
    RETURN 'ERROR;'||SQLERRM;
END pago_adelantado;


--//////////////////////////////////////////////////////////////////////////////
--Funcion para pagar el total de la deuda
--//////////////////////////////////////////////////////////////////////////////
FUNCTION pago_periodo(
    id IN tx_yanbal.cns%type, 
    periodo IN VARCHAR2
)RETURN VARCHAR2 IS
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN 
    RETURN 'Esta operacion no esta soportada.';
END pago_periodo;

END pck_yanbal;
/

