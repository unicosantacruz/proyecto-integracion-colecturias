/*******************************************************************************
HISTORICO
21-07-2014  Fernando Flores Creacion.
01-08-2014  Fernando Flores Actualizacion.
*******************************************************************************/

/*******************************************************************************
configuracion inicial para yanbal
*******************************************************************************/
DECLARE    
    ----------------------------------------------------------------------------
    -- codigo del servicio
    ----------------------------------------------------------------------------
    varCodServicio VARCHAR2(50) := '64';
    ----------------------------------------------------------------------------
    varIdGeneral NUMBER(9,0);
    cursor cursor1(varCodServicio VARCHAR2) is
        SELECT id_pago FROM unico.di_pagos where cod_servicio=varCodServicio;
    cursor cursor2(varCodServicio VARCHAR2) is
        SELECT id_exportacion FROM unico.di_exportacion where cod_servicio=varCodServicio;
    cursor cursor3(varCodServicio VARCHAR2) is
        SELECT id_importacion FROM unico.di_importacion where cod_servicio=varCodServicio;
    cursor cursor4 is
        SELECT id_conexion FROM unico.di_conexion where nombre='Colecturia de deudas';

BEGIN

    BEGIN    
        SELECT s.cod_servicio INTO varCodServicio FROM bc_empresa a, bc_servicio s WHERE rownum<=1 AND a.cod_empresa=s.cod_empresa AND a.descripcion='YANBAL';
        dbms_output.put_line('Codigo servicio obtenido');
    EXCEPTION WHEN others THEN 
        dbms_output.put_line('ERROR: No pudo obtenerse el codigo servicio.');
        RETURN;
    END;

    DELETE FROM tx_yanbal;
    COMMIT;
    dbms_output.put_line('pagos borrado');
    
    DELETE FROM di_tipo_cambio;
    COMMIT;
    dbms_output.put_line('tipos de cambio borrado');
    
    DELETE FROM di_email_servicio;
    COMMIT;
    DELETE FROM di_email;
    COMMIT;
    dbms_output.put_line('emails borrado');
    
    FOR x IN cursor1(varCodServicio) LOOP
        varIdGeneral:=x.id_pago;
        SELECT id_pago INTO varIdGeneral FROM unico.di_pagos where cod_servicio=varCodServicio;
        DELETE FROM unico.tx_pagos WHERE id_pago=varIdGeneral;
        COMMIT;
        DELETE FROM unico.di_claves WHERE id_pago=varIdGeneral;
        COMMIT;
        DELETE FROM unico.di_pagos WHERE id_pago=varIdGeneral;
        COMMIT;
    END LOOP;
    dbms_output.put_line('pagos borrado');
    
    FOR x IN cursor2(varCodServicio) LOOP
        varIdGeneral:=x.id_exportacion;
        DELETE FROM unico.tx_exportacion WHERE id_exportacion=varIdGeneral;
        COMMIT;
        DELETE FROM unico.di_exportacion WHERE id_exportacion=varIdGeneral;
        COMMIT;
    END LOOP;
    dbms_output.put_line('exportacion borrado');
    
    FOR x IN cursor3(varCodServicio) LOOP
        varIdGeneral:=x.id_importacion;
        DELETE FROM unico.tx_importacion WHERE id_importacion=varIdGeneral;
        COMMIT;
        DELETE FROM unico.di_importacion WHERE id_importacion=varIdGeneral;
        COMMIT;    
    END LOOP;
    dbms_output.put_line('importacion borrado');
    
    FOR x IN cursor4 LOOP
        varIdGeneral:=x.id_conexion;
        DELETE FROM unico.di_atributo_conexion WHERE id_conexion=varIdGeneral;
        COMMIT;
        DELETE FROM unico.di_conexion WHERE id_conexion=varIdGeneral;
        COMMIT;
    END LOOP;
    dbms_output.put_line('conexion borrado');

    dbms_output.put_line('OK');
    
EXCEPTION
    WHEN others THEN
    ROLLBACK;
    dbms_output.put_line('EXCEPTION:'||SQLERRM);
END;
/

DROP PACKAGE BODY unico.pck_yanbal;
DROP PACKAGE unico.pck_yanbal;
DROP TABLE unico.tx_yanbal;
/


/*******************************************************************************
ELIMINACION DEL SISTEMA
*******************************************************************************/

DROP TABLE unico.tx_pagos_error;
DROP TABLE unico.tx_pagos;
DROP TABLE unico.tx_importacion;
DROP TABLE unico.tx_exportacion;
DROP TABLE unico.di_claves;
DROP TABLE unico.di_pagos;
DROP TABLE unico.di_importacion;
DROP TABLE unico.di_exportacion;
DROP TABLE unico.di_atributo_conexion;
DROP TABLE unico.di_conexion;
DROP TABLE unico.di_email_servicio;
DROP TABLE unico.di_email;
DROP TABLE unico.di_tipo_cambio;
/

DROP SEQUENCE unico.sec_exportacion;
DROP SEQUENCE unico.sec_importacion;
DROP SEQUENCE unico.sec_id_pago;
DROP SEQUENCE unico.sec_id_exportacion;
DROP SEQUENCE unico.sec_id_importacion;
DROP SEQUENCE unico.sec_id_conexion;
DROP SEQUENCE unico.sec_id_email;
/

